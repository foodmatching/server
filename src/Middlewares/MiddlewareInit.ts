import IngredientMiddleware from "./IngredientMiddleware";
import * as core from "express-serve-static-core";
import bodyParser from "body-parser";
import cors from "cors";
import { Logger } from "../Logs/WinstonLogger";

export default class MiddlewareInit {

    public static init(app: core.Express): void {
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(cors());

        // app.use(function(req, res, next) {
        //     Logger.info("endpoint accessed: " + req.path);
        // });

        // app.use('/Ingredient/AddIngredient',(req, res) =>IngredientMiddleware.validateReq(req,res));
    }
}