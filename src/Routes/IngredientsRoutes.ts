import * as core from "express-serve-static-core";
import IngredientController from "./../Controllers/IngredientController";
import FileUploader from "./../Utils/FileUploader";

export default class IngredientsRoutes {

    public init(app: core.Express): void {
        app.post('/Ingredient/AddIngredient',(req, res) =>IngredientController.addIngredient(req,res));
        app.post('/Ingredient/AddIngredients',(req, res) =>IngredientController.addIngredients(req,res));
        app.post('/Ingredient/GetAllIngredients',(req, res) =>IngredientController.getAllIngredients(req,res));
        app.post('/Ingredient/GetIngredientByName',(req, res) =>IngredientController.getIngredientByName(req,res));
        app.post('/Ingredient/GetIngredientByID',(req, res) =>IngredientController.getIngredientByID(req,res));
        app.post('/Ingredient/DeleteIngredient',(req, res) =>IngredientController.deleteIngredientById(req,res));
        app.post('/Ingredient/GetIngredientStartWith',(req,res)=>IngredientController.getIngredientStartWith(req,res));
        app.post('/Ingredient/DeleteIngredientByName',(req, res) =>IngredientController.deleteIngredientByName(req,res));  
        app.post('/Ingredient/addBadFor',(req, res) =>IngredientController.addBadFor(req,res)); 
        app.post('/Ingredient/addGoodFor',(req, res) =>IngredientController.addGoodFor(req,res)); 
        app.post('/Ingredient/addIngredientPicture', FileUploader.upload("ingredient").single('image'), (req,res)=>IngredientController.addIngredientPicture(req,res));
    }
}
