import * as core from "express-serve-static-core";
import UserController from "../Controllers/UsersController";
import FileUploader from "../Utils/FileUploader";

export default class UserRoutes {

    public init(app: core.Express): void {
        app.post('/User/Connect',(req, res) =>UserController.connect(req,res));
        app.post('/User/AddNewUser',(req, res) =>UserController.addNewUser(req,res));
        app.post('/User/AddNewUsers',(req, res) =>UserController.addNewUsers(req,res));
        app.post('/User/DeleteUserByUserName',(req, res) =>UserController.deleteUserByUserName(req,res));
        app.post('/User/DeleteUserById',(req, res) =>UserController.deleteUserById(req,res));
        app.post('/User/GetAllUsers',(req, res) =>UserController.getAllUsers(req,res));
        app.post('/User/getUserByFacebook',(req, res) =>UserController.getUserByFacebook(req,res));  
        app.post('/User/getUserByGmail',(req, res) =>UserController.getUserByGmail(req,res));
        app.post('/User/getUserByID',(req, res) =>UserController.getUserByID(req,res));
        app.post('/User/getUserNameAndPictureByID',(req, res)  =>UserController.getUserNameAndPictureByID(req,res));
        app.post('/User/getUserByUserName',(req, res) =>UserController.getUserByUserName(req,res));
        app.post('/User/addHealthIssues',(req,res)=>UserController.addHelthIssues(req,res));
        app.post('/User/addIngredientsIDsLikes',(req,res)=>UserController.addIngredientsIDsLikes(req,res));
        app.post('/User/addTypesLikes',(req,res)=>UserController.addTypesLikes(req,res));
        app.post('/User/addUserPicture', FileUploader.upload("users").single('image'), (req,res)=>UserController.addUserPicture(req,res));
        app.post('/User/addCookBook',(req,res)=>UserController.addCookBook(req,res));
        app.post('/User/deleteCookBook',(req,res)=>UserController.deleteCookBook(req,res));
        app.post('/User/addRecipeToCookBook',(req,res)=>UserController.addRecipeToCookBook(req,res));
        app.post('/User/removeRecipeToCookBook',(req,res)=>UserController.removeRecipeToCookBook(req,res));
        app.post('/User/getAllCookBooks',(req,res)=>UserController.getAllCookbooks(req,res));
        app.post('/User/getAllRecipesInCookBook',(req,res)=>UserController.getAllRecipesInCookBook(req,res));
        app.post('/User/getAllUserRecipes',(req,res)=>UserController.getAllUserRecipes(req,res));
        app.post('/User/getRecipesMatchForUser',(req,res)=>UserController.getRecipesMatchForUser(req,res));
        app.post('/User/isRecipeRecommendedForUser',(req,res)=>UserController.isRecipeRecommendedForUser(req,res));
        app.post('/User/isIngredientRecommendedForUser',(req,res)=>UserController.isRecipeRecommendedForUser(req,res));
    }
}
