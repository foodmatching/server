import * as core from "express-serve-static-core";
import IngredientsRoutes from "./IngredientsRoutes";
import RecipesRoutes from "./RecipesRoutes";
import UsersRoutes from "./UsersRoutes";
export default class RoutesInit {

    public initRouts(app: core.Express): void {
       
        let ingredientRoutes = new IngredientsRoutes();
        ingredientRoutes.init(app);
        let recipesRoutes = new RecipesRoutes();
        recipesRoutes.init(app);
        let usersRoutes = new UsersRoutes();
        usersRoutes.init(app);

    }
}