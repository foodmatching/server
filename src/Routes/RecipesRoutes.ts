import * as core from "express-serve-static-core";
import RecipeController from "./../Controllers/RecipeController";
import FileUploader from "./../Utils/FileUploader";

export default class RecipeRoutes {

    public init(app: core.Express): void {
        app.post('/Recipe/AddRecipe',(req, res) =>RecipeController.addRecipe(req,res));
        app.post('/Recipe/AddRecipes',(req, res) =>RecipeController.addRecipes(req,res));
        app.post('/Recipe/GetAllRecipes',(req, res) =>RecipeController.getAllRecipes(req,res));
        app.post('/Recipe/GetRecipeByName',(req, res) =>RecipeController.getRecipeByName(req,res));
        app.post('/Recipe/GetRecipeByID',(req, res) =>RecipeController.getRecipeByID(req,res));
        app.post('/Recipe/DeleteRecipe',(req, res) =>RecipeController.deleteRecipeById(req,res));
        app.post('/Recipe/DeleteRecipeByName',(req, res) =>RecipeController.deleteRecipeByName(req,res));  
        app.post('/Recipe/AddComment',(req, res) =>RecipeController.addCommentToRecipe(req,res));
        app.post('/Recipe/getAllCommentsForRecipe',(req, res) =>RecipeController.getAllCommentsForRecipe(req,res));
        app.post('/Recipe/getIngredientsForRecipe',(req, res) =>RecipeController.getIngredientsForRecipe(req,res));
        app.post('/Recipe/GetRecipesContaining',(req,res)=>RecipeController.getRecipesContaining(req,res));
        app.post('/Recipe/GetRecipesContainingNamesAndIds',(req,res)=>RecipeController.getRecipesContainingNamesAndIds(req,res));
        app.post('/Recipe/addRecipePicture', FileUploader.upload("recipes").single('image'), (req,res)=>RecipeController.addRecipePicture(req,res));
        app.post('/Recipe/addRecipeCommentPicture', FileUploader.upload("recipes/comments").single('image'), (req,res)=>RecipeController.addRecipeCommentPicture(req,res));
    }
}
