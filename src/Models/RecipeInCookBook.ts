import { ObjectID } from "bson";

export default class RecipeInCookBook{
    addedtime: Date;
    recipeId: ObjectID;

    constructor(recipeId:ObjectID){
        this.recipeId = recipeId;
        this.addedtime = new Date();
    }
}