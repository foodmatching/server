import mongoose from "mongoose";
import { ObjectID } from "bson";
export type RecipeModel = mongoose.Document & {
  name: string;
  rank:number,
  ingredients: {
    ingredientId:ObjectID
    amount:number
    unit:MeasurementUnit
  }[];
  nutritionValues: NutritionValues;
  instructions: string;
  comments: Comment[];
  type:string;
  picture: { data: Buffer, contentType: string };
};


export type Comment = mongoose.Document & {
  userID:ObjectID;
  authorName: String;
  text: String;
  rank:number;
  picture: { data: Buffer, contentType: String };
  time: Date;
};

export type NutritionValues = mongoose.Document & {
  caloricValue: number,
  carbohydrates: number,
  fats: number,
  sugar: number,
  saturatedFat: number,
  cholesterol : number
};

const RecipeSchema = new mongoose.Schema({
  name: String,
  ingredients:[{
    ingredientId:ObjectID,
    amount:Number,
    unit: String
  }],
  nutritionValues: {
    caloricValue: Number,
    carbohydrates: Number,
    fats: Number,
    sugar: Number,
    saturatedFat: Number,
    cholesterol : Number
  },
  instructions: String,
  rank:Number,
  type:String,
  picture: { data: Buffer, contentType: String }, 
  comments: [{
    userID:ObjectID,
    authorName: String,
    text: String,
    rank:Number,
    picture: { data: Buffer, contentType: String }, 
    time:Date
  }],
}, { timestamps: true })

const Recipe = mongoose.model("recipe", RecipeSchema);
export default Recipe;