//import bcrypt from "bcrypt-nodejs";
import crypto from "crypto";
import mongoose from "mongoose";
import CookBook from "./Cookbook";
import { HealthIssues } from "./HealthIssues";
import { ObjectID } from "bson";

export type UserModel = mongoose.Document & {
  
  username: string,
  password: string,
  email: string,
  facebook: string,
  gmail:string,
  
  profile: {
    name: string,
    gender: string,
    picture?: { data: Buffer, contentType: String }, 
    height: Number,
    weight:Number,
    dateOfBirth:Date,
    whatsLookingFor: string,
    healthIssues: HealthIssues[]
    ingredientsIDsLikes:ObjectID[]
    typesLikes:string[]
  },

  cookBooks:CookBook[]
};

const userSchema = new mongoose.Schema({
  email: { type: String, unique: true },
  username: String,
  password: String,
  facebook: String,
  gmail:String,

  profile: {
    name: String,
    gender: String,
    picture: { data: Buffer, contentType: String }, 
    height: Number,
    weight:Number,
    dateOfBirth:Date,
    whatsLookingFor: String,
    healthIssues: Array, 
    ingredientsIDsLikes:Array,
    typesLikes:Array
  },
  cookBooks:[{
    name: String,
    recipes: Array
  }]
}, { timestamps: true });

/**
 * Password hash middleware.
 */
// userSchema.pre("save", function save(next) {
//   const user = this;
//   if (!user.isModified("password")) { return next(); }
//   bcrypt.genSalt(10, (err, salt) => {
//     if (err) { return next(err); }
//     bcrypt.hash(user.password, salt, undefined, (err: mongoose.Error, hash) => {
//       if (err) { return next(err); }
//       user.password = hash;
//       next();
//     });
//   });
// });

/**
 * Helper method for getting user's gravatar.
 */
// userSchema.methods.gravatar = function (size: number) {
//   if (!size) {
//     size = 200;
//   }
//   if (!this.email) {
//     return `https://gravatar.com/avatar/?s=${size}&d=retro`;
//   }
//   const md5 = crypto.createHash("md5").update(this.email).digest("hex");
//   return `https://gravatar.com/avatar/${md5}?s=${size}&d=retro`;
// };

// export const User: UserType = mongoose.model<UserType>('User', userSchema);
const User = mongoose.model("User", userSchema);
export default User;