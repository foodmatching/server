
export enum HealthIssues {
    LACTOSE_INTOLERANCE = "Lactose intolerance",
    DIABETES = "diabetes",
    GLUTEN_INTOLERANCE = "gluten intolerance",
    CELIAC = "Celiac",
    VEGETERIAN = "Vegeterian",
    VEGAN = "Vegan"
    HIGH_CHOLESTEROL = "High Cholesterol"
}

export function getHealthIssuesByIndex(i: number): HealthIssues {
    switch (i) {
        case 0:
            return HealthIssues.LACTOSE_INTOLERANCE;
        case 1:
            return HealthIssues.DIABETES;
        case 2:
            return HealthIssues.GLUTEN_INTOLERANCE;
        case 3:
            return HealthIssues.VEGITERIAN;
        case 4:
            return HealthIssues.VEGAN;
        case 5:
            return HealthIssues.HIGH_CHOLESTEROL;
        default:
            return HealthIssues.CELIAC;
    }
}
