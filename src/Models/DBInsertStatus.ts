/**
 * The status of adding the data to the db
 */
export enum Status {
  seccseed,
  DataExisted,
  NoDataToSave,
  error
}