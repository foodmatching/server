import RecipeInCookBook from "./RecipeInCookBook";

export default class CookBook{
    name: String;
    recipes: Array<RecipeInCookBook>;

    constructor(name:string){
        this.name = name;
        this.recipes = new Array<RecipeInCookBook>();
    }
}
