import Recipe, { RecipeModel } from "./Recipe";
import RecipeInCookBook from "./RecipeInCookBook";
import { Logger } from "../Logs/WinstonLogger";
import LogMessages from "../Utils/LogMessages";

export default class RecipeInCookBookOutput{
    addedtime: Date;
    recipe: RecipeModel;

    static async build(recipe:RecipeInCookBook):Promise<RecipeInCookBookOutput>{
        let recipeInCookBookOutput: RecipeInCookBookOutput = new RecipeInCookBookOutput();
        try{
            recipeInCookBookOutput.recipe = await Recipe.findById(recipe.recipeId).exec() as RecipeModel;
        }catch(err){
            Logger.error(LogMessages.ERROR, err);
            throw err;
        }
        recipeInCookBookOutput.addedtime = recipe.addedtime;

        return recipeInCookBookOutput;
    }

    public static sort(obj1:RecipeInCookBookOutput, obj2:RecipeInCookBookOutput) {
        if(!obj1.addedtime && obj2.addedtime){
            return 0;
        }
        if (obj1.addedtime < obj2.addedtime) {
            return 1;
        }
    
        if (obj1.addedtime > obj2.addedtime) {
            return -1;
        }
    
        return 0;
    }
}