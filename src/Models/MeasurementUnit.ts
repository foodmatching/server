const enum MeasurementUnit{
    TABLE_SPOON = "table spoon",
    TEA_SPOON = "tea spoon",
    CUP = "cup",
    ML = "ml",
    MG = "mg",
    G = "g",
    UNIT = "unit"

}