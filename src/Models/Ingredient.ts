import mongoose from "mongoose";
import { HealthIssues } from "./HealthIssues";

export type IngredientModel = mongoose.Document & {
    name: string;
    dietaryComponents:{
        caloricValue: number;
        carbohydrates: number;
        fats: number;
        sugar: number;
        saturatedFat: number;
        cholesterol : number;
    }
    goodFor: HealthIssues[];
    badFor: HealthIssues[];
    picture: { data: Buffer, contentType: string };
}

const IngredientSchema = new mongoose.Schema({
    name: { type: String, unique: true, required: true,key: true},
    dietaryComponents:{
        caloricValue: Number,
        carbohydrates: Number,
        fats: Number,
        sugar: Number,
        saturatedFat: Number,
        cholesterol : Number
    },
    goodFor: Array,
    badFor: Array,
    picture: { data: Buffer, contentType: String }
});
const Ingredient = mongoose.model("Ingredient", IngredientSchema);
export default Ingredient;