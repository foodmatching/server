import * as winston from "winston";
import * as os from "os";
import { existsSync, mkdirSync } from "fs";
import LoggerConfig from "../Configuration/LoggerConfig";
import path from 'path';
import { Format } from "logform";
    export class Logger {
        public static logger: winston.Logger; 
        private static myFormat():Format {
            return winston.format.printf(info=>{
                if(info instanceof Error) {
                    return Logger.dateToString(new Date()) +` [${info.level}] ${info.message} ${info.stack}`;
                }
                else{
                    return Logger.dateToString(new Date()) +` [${info.level}] ${info.message}`;
                }
            })
        }
        public static InitLogger(loggerConfig: LoggerConfig, projectPath:string){
            let logDirLocation = path.join(projectPath,loggerConfig.fileLocation);
            if (!existsSync(logDirLocation)){
                mkdirSync(logDirLocation,{recursive:true});
            }
            let logLocation = path.join(logDirLocation,loggerConfig.fileName);
            console.log("logLocation", logLocation);
            Logger.logger = winston.createLogger({
                
                format: Logger.myFormat(),
                transports: [
                    new winston.transports.Console({}),
                    new winston.transports.File (
                        {
                          level: 'info',
                          filename: logLocation,
                          handleExceptions: true,
                          maxsize: loggerConfig.fileMaxSize, 
                          maxFiles: loggerConfig.maxFiles,
                        }
                      )
                ],
                exitOnError: false,
            });
        }
        
        public static info(message: string){
            Logger.logger.info(message);
        }

        public static warn(message: string){
            Logger.logger.warn(message); 
        }

        public static error(message: string, err: Error){
            Logger.logger.error(message);
            Logger.logger.error(err);
            
        }
        
        public static debug(message: string){
            Logger.logger.debug(message);
        }

        private static pad(num: number,destinationLength:number){
            if(num === undefined || num === null){
                throw new Error("num cant be undified");
            }
            let numAsString = num.toString();
            for(let i = numAsString.length; i<destinationLength; i++){
                numAsString ="0"+numAsString;
            }
            return numAsString;
        }
        private static dateToString(date:Date): String{
            const dayOfMonth = Logger.pad(date.getDate(),2);
            const month = Logger.pad(date.getMonth() + 1,2);
            const year = date.getFullYear().toString();
            const hours = Logger.pad(date.getHours(),2);
            const minutes = Logger.pad(date.getMinutes(),2);
            const seconds = Logger.pad(date.getSeconds(),2);
            const fullDate = dayOfMonth + "-" + month + "-" + year + " " 
            + hours + ":" + minutes + ":" + seconds;
            return fullDate;
        }
         
    }
    
