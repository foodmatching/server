import axios from 'axios';

import { IngredientModel } from '../Models/Ingredient';
import { RecipeModel } from '../Models/Recipe';
import IngredientService from '../services/IngredientService';
import { Logger } from '../Logs/WinstonLogger';
import LogMessages from './LogMessages';
import ImageFinder from './ImageFinder';
import Axios from 'axios';
import Path from 'path';

const NUTRITIONIX_APP_CREDS = [
    {
        NUTRITIONIX_APP_KEY:  "92acdc4317a36a2324af8761c5cd8ed4",
        NUTRITIONIX_APP_ID:  "8dc226ad"
    },
    {
        NUTRITIONIX_APP_KEY:  "5f723809c8b2d6aca683cc13f8c31748",
        NUTRITIONIX_APP_ID:  "e9631acd"
    },
    {
        NUTRITIONIX_APP_KEY:  "721eeff998225d173f2632efd4e48d8e",
        NUTRITIONIX_APP_ID:  "63a408bf"
    },
    {
        NUTRITIONIX_APP_KEY:  "24a008b4b560f66fd7dd27968c22d23b",
        NUTRITIONIX_APP_ID:  "ef0a977e"
    },
    {
        NUTRITIONIX_APP_KEY:  "083e5a00843ed997ffeaca0e0cc16292",
        NUTRITIONIX_APP_ID:  "69335b27"
    }
];
let credIndex = 0;

// returns null if not found
export async function getIngredientDataFromAPI(name: string): Promise<IngredientModel | null> {
    let resultFoods = await searchNutritionApi("100 g " + name);

    if (resultFoods === null) {
        return null;
    }
    const resultFood = resultFoods[0];

    const ingredient: IngredientModel = await resultFoodToIngredientModel(resultFood);

    return ingredient;
}

export async function calculateRecipeNutrition(recipe: RecipeModel) {
    let query = "";
    for (let ingredient of recipe.ingredients) {
        const ingredientData = await IngredientService.getIngredientByObjectID(ingredient.ingredientId);
        query += `${ingredient.amount} ${ingredient.unit} ${ingredientData.name} ,`;
    }
    // TODO handle null and errors
    const resultFoods = await searchNutritionApi(query);
    setBasicNutritionValues(recipe);
    for (let resultFood of resultFoods) {
        const ingredientData: IngredientModel = await resultFoodToIngredientModel(resultFood);
        recipe.nutritionValues.caloricValue += ingredientData.dietaryComponents.caloricValue;
        recipe.nutritionValues.carbohydrates += ingredientData.dietaryComponents.carbohydrates;
        recipe.nutritionValues.fats += ingredientData.dietaryComponents.fats;
        recipe.nutritionValues.sugar += ingredientData.dietaryComponents.sugar;
        recipe.nutritionValues.saturatedFat += ingredientData.dietaryComponents.saturatedFat;
        recipe.nutritionValues.cholesterol += ingredientData.dietaryComponents.cholesterol;
    }
}
function setBasicNutritionValues(recipe: RecipeModel){
    recipe.nutritionValues.caloricValue = 0;
    recipe.nutritionValues.carbohydrates = 0;
    recipe.nutritionValues.fats = 0;
    recipe.nutritionValues.sugar = 0;
    recipe.nutritionValues.saturatedFat = 0;
    recipe.nutritionValues.cholesterol = 0;
}
async function searchNutritionApi(query: string): Promise<any> {
    let res;
    try {
        const url = 'https://trackapi.nutritionix.com/v2/natural/nutrients';
        const headers = {
            Host: 'trackapi.nutritionix.com',
            Referer: 'https://trackapi.nutritionix.com/docs/',
            accept: 'application/json',
            'Content-Type': 'application/json',
            'x-app-key': NUTRITIONIX_APP_CREDS[credIndex].NUTRITIONIX_APP_KEY,
            'x-app-id': NUTRITIONIX_APP_CREDS[credIndex].NUTRITIONIX_APP_ID,
            Connection: 'keep-alive',
            Origin: 'https://trackapi.nutritionix.com'
        };
        credIndex ++;
        if (credIndex === NUTRITIONIX_APP_CREDS.length) {
            credIndex = 0;
        }

        const body = { query: query, use_branded_foods: false, locale: 'en_US' };

        res = await axios.post(url, body, {headers: headers});

    } catch(e) {
        if(e.response.status === 404) {
            return null;
        }
        if(e.response.status === 401 && e.response.data.message === "usage limits exceeded") {
            Logger.error("\n\n --- NUTRITIONIX - usage limits exceeded --- \n\n", e);
            throw e;
        }
        Logger.error("searchNutritionApi error, query = " + query, e);
        throw e;
    }
    return res.data.foods;
}

async function resultFoodToIngredientModel(resultFood: any): Promise<IngredientModel> {
    return {
        name: resultFood.food_name,
        dietaryComponents: {
            caloricValue: resultFood.nf_calories,
            carbohydrates: resultFood.nf_total_carbohydrate,
            fats: resultFood.nf_total_fat,
            sugar: resultFood.nf_sugars,
            saturatedFat: resultFood.nf_saturated_fat,
            cholesterol: resultFood.nf_cholesterol
        },
        goodFor: [],
        badFor: [],
        picture: await getPictureFormatFromNutritionixUrl(resultFood.photo.thumb)
    } as any as IngredientModel;
}


async function getPictureFormatFromNutritionixUrl(url: string):Promise<{ data: Buffer, contentType: string }> {
    // read binary data
    var imageBase64Buffer = await Axios.get(url, { responseType: 'arraybuffer' });
    // convert binary data to base64 encoded string
    const photoData = new Buffer(imageBase64Buffer.data, "binary");

    const contentType = "image/" + Path.extname(url).split(".")[1];

    return {data: photoData, contentType: contentType}
}

