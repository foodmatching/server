import multer = require("multer");
import { existsSync, mkdirSync } from "fs";
import path from 'path';

export default class FileUploader {
    private static projectPath:string;
    static init(projectPath: string){
        FileUploader.projectPath = projectPath;
    }
    public static upload(destinationPath: string): multer.Instance {
        return multer({storage: multer.diskStorage({
            destination: (req, file, cb) => {
                const dir: string = path.join(FileUploader.projectPath ,'uploads' , destinationPath);
                if (!existsSync(dir)){
                    mkdirSync(dir,{recursive:true});
                }
                cb(null, dir);
            },
            filename: (req, file, cb) => {
                cb(null, Date.now() + "_" + file.originalname);
            }
        })});
    }
}
