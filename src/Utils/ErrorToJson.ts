export default class ErrorToJson {
    public static convert(err: Error){
        let stringToReturn = "{";
        stringToReturn = stringToReturn.concat("\"name\":\"").concat(err.name).concat("\",")
                      .concat("\"message\":\"").concat(err.message).concat("\",")
                      .concat("\"stack\":\"").concat(err.stack).concat("\"")
                      .concat("}");
        return stringToReturn;
    }
}