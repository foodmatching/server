export default class LogMessages{
    static readonly SERVER_IS_UP: string = "The server is UP!";
    static readonly SERVER_LISTENING: string = "app listening on port 3000!";
    static readonly SERVER_START: string = "Start loading server";
    static readonly SERVER_STOPPED: string = "Server stopped";
    static readonly MONGO_CONNECTION_ESTABLISHED: string = "Connected to mongo";
    static readonly UPDATE_SECCSEDED: string = "updete seccseded";
    
    //Ingredients
    static readonly NO_INGREDIENTS: string = "there are no ingredients ";
    static readonly INGREDIENT_NOT_EXIST: string = "Cannot find the specipied ingredient: ";
    static readonly DELETE_INGREDIENT_SECCSEED: string = "delete of ingedient seccseed";
    static readonly ADD_INGREDIENT_SECCSEED: string = "The ingredient added sccessfuly";
    static readonly ADD_INGREDIENTS_SECCSEED: string = "The ingredients added sccessfuly";
    static readonly INGREDIENT_ALREADY_EXIST: string ="The given ingredient is already exist";
    
    //Recipes
    static readonly NO_RECIPES: string = "there are no recipes";
    static readonly RECIPE_NOT_EXIST: string = "Cannot find the specipied recipe: ";
    static readonly DELETE_RECIPE_SECCSEED: string = "delete of recipe seccseed";
    static readonly ADD_RECIPE_SECCSEED: string = "The recipe added sccessfuly";
    static readonly RECIPE_ALREADY_EXIST: string ="The given recipe is already exist";
    //Users
    static readonly NO_USERS: string = "there are no users ";
    static readonly USER_NOT_EXIST: string = "Cannot find the specipied user: ";
    static readonly COOKBOOK_NOT_EXIST: string = "Cannot find the specipied cook book: ";
    static readonly USER_DONT_LIKES_INGREDIENTS: string = "User doesn't like any ingredients";
    static readonly USER_DONT_LIKES_TYPES: string = "user doesn't like any types of food";
    static readonly RECIPE_ALLREADY_IN_COOKBOOK: string = "This recipe is already in this cook book!";
    static readonly COOKBOOK_ALLREADY_EXIST: string = "There is already a cook book in this name!"
    static readonly TYPE_ALLREADY_LIKED: string = "this type is allready liked";
    static readonly TYPE_LIKED_EMPTY: string = "type likes cant be empty!";
    static readonly INGREDIENT_LIKED_EMPTY: string = "ingredient likes cant be empty!";
    static readonly INGREDIENT_ALLREADY_LIKED: string = "this ingredient is allready liked";
    static readonly HEALTH_ISSUE_ALLREADY_MARKED: string = "this health issue is allready markd";
    static readonly HEALTH_ISSUE_EMPTY: string = "health issue cant be empty!";
    static readonly ADD_USER_SECCSEED: string = "The user added sccessfuly";
    static readonly USER_ALREADY_EXIST: string = "The given user is already exist";
    static readonly CONNECT: string = "connect seccessfuly!";
    static readonly DELETE_USER_SECCSEED: string = "delete of user seccseed";
    // Errors
    static readonly MONGO_CONNECTION_ERROR: string = "MongoDB connection error. Please make sure MongoDB is running. ";
    static readonly ERROR_GETING_INGREDIENTS: string = "An error has occured, can't get the ingredients. Error: " ;
    static readonly ERROR_GETING_INGREDIENT: string = "An error has occured. Cannot find the specipied ingredient: ";
    static readonly ERROR: string = "an Unexpected error occourd ";
    static readonly FILE_NOT_FOUND: string = "this request missing a file!";
    static readonly ERROR_GETING_RECIPES: string = "An error has occured can't get the recipes. Error: "
    static readonly ERROR_GETING_RECIPE: string = "An error has occourd. Cann't find the specipied recipe: "
    static readonly NO_DATA = "can't save this without data";
    static readonly INCORRECT_PASSWORD: string = "this user or password is inncorect";
    static readonly ILLEGAL_PAGE_NUMBER: string = "invalid page number, should start with 1";
    static readonly CANT_DELETE: string ="Error in delete this entity";
}