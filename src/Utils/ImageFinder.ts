import Axios from "axios";
import { Logger } from "../Logs/WinstonLogger";
import LogMessages from "./LogMessages";
import path from 'path';

export default class ImageFinder {
    public static async searchPicture(query: string): Promise<{ data: Buffer, contentType: string }> {
        query = query.replace(/%/g,' '); // remove % from query

        Logger.info("searching picture for q=" + query);
        let ret = undefined;
        try {
            // joe
            // const googleKey1 = "AIzaSyCa2yqmVyxadG4DbkKxWsEhHIINRgpoDSI";
            // const cx = "005255256755311594623:4touqusmymi";
            // const cx = "005255256755311594623:6tybzc6eyw0";
            // amit
            // const googleKey1 = "AIzaSyC1Xj7Zlx-Q9QXBx0VuO94ExwN1y7osovA";
            // const cx = "008055038869238481645:i6vmucbhgpc";
            // const googleKey1 = "AIzaSyCKngNzVkl-EILoG9IYo9dJUmds1JYBogE";
            const cx = "018001512922295891083:baxkfq9hjmu";
            // const googleKey1 = "AIzaSyAiuIsRVQ5NmdTaMepr5gBdZsj9hsBY054";
            // const googleKey1 = "AIzaSyA_0WQt0zVyTdmh9RMWSLNyQA12sD3WUIM";
            const googleKey1 = "AIzaSyBe-cGAmDHpCkwBzfL6RSeFhLBmxG_qCzM";

            const url = `https://www.googleapis.com/customsearch/v1?key=${googleKey1}&cx=${cx}&searchType=image&q=${query}`;
            const res = await Axios.get(url);
            const img = res.data.items[0].image.thumbnailLink
            ret = await ImageFinder.getPictureFormatFromGoogleUrl(img);
        } catch(e) {
            Logger.error("ImageFinder searchPicture error! query = " + query, e);
        }
        return ret;
    }
    public static async getPictureFormatFromGoogleUrl(image: string):Promise<{ data: Buffer, contentType: string }> {
        // read binary data
        var imageBase64Buffer = await Axios.get(image, { responseType: 'arraybuffer' });
        // convert binary data to base64 encoded string
        const photoData = new Buffer(imageBase64Buffer.data, "binary");
        return { data: photoData, contentType: "image/jpeg"};
    }

    // public static async searchPictureOldShit(query: string): Promise<{ data: Buffer, contentType: string }> {

    //     query = query.replace(/,/g,' '); // remove , from query
    //     query = query.replace(/w\//g,' '); // remove w/ from query
    //     query = query.replace(/\//g,' '); // remove / from query

    //     let res;
    //     try {
    //         const url= 'https://api.imgur.com/3/gallery/search/top/';
    //         const headers = {
    //             Authorization: 'Client-ID bfb843ab5743e41'//3cf1f877142f694'
    //         };

    //         let qs: any = { q: 'title: '+query, q_exactly: '', q_type: [ 'jpg', 'png' ], q_size_px: "small" };
    //         res = await Axios.get(url, {headers: headers,params:qs});
    //         if(res && res.data && res.data.data[0]) {
    //             const image = res.data.data[0].is_album ? res.data.data[0].images[0] : res.data.data[0];
    //             if (image.size < 500000) {
    //                 return await ImageFinder.getPictureFormatFromUrl(image);
    //             }
    //         }

    //         qs = { q: 'title: ' + query, q_all: '', q_type: [ 'jpg', 'png' ], q_size_px: "small" };
    //         res = await Axios.get(url, {headers: headers,params:qs});
    //         if(res && res.data && res.data.data[0]) {
    //             const image = res.data.data[0].is_album ? res.data.data[0].images[0] : res.data.data[0];
    //             if (image.size < 500000) {
    //                 return await ImageFinder.getPictureFormatFromUrl(res.data.data[0].is_album ? res.data.data[0].images[0] : res.data.data[0]);
    //             }
    //         }

    //         qs = { q: 'title: '+query, q_type: [ 'jpg', 'png' ], q_size_px: "small" };
    //         res = await Axios.get(url, {headers: headers,params:qs});
    //         if(res && res.data && res.data.data[0]) {
    //             const image = res.data.data[0].is_album ? res.data.data[0].images[0] : res.data.data[0];
    //             if (image.size < 500000) {
    //                 return await ImageFinder.getPictureFormatFromUrl(res.data.data[0].is_album ? res.data.data[0].images[0] : res.data.data[0]);
    //             }
    //         }

    //         let string = "";
    //         let stirngs :Array<string> = query.split(" ");
    //         for(let i =0; i<Math.floor(stirngs.length/2);i++){
    //             string += stirngs[i] +" ";
    //         }
    //         qs = { q: 'title: '+string, q_any: '', q_type: [ 'jpg', 'png' ], q_size_px: "small" };
    //         res = await Axios.get(url, {headers: headers,params:qs});
    //         if(res && res.data && res.data.data[0]) {
    //             const image = res.data.data[0].is_album ? res.data.data[0].images[0] : res.data.data[0];
    //             if (image.size < 500000) {
    //                 return await ImageFinder.getPictureFormatFromUrl(res.data.data[0].is_album ? res.data.data[0].images[0] : res.data.data[0]);
    //             }
    //         }
    //     } catch(e) {
    //         Logger.error("ImageFinder.searchPicture - error searching picture in imgur api! query = " + query, e);
    //         throw e;
    //     }
    //     return undefined;
    // }

    // public static async getPictureFormatFromUrl(image: {type:string, link:string}):Promise<{ data: Buffer, contentType: string }> {
    //     // read binary data
    //     var imageBase64Buffer = await Axios.get(image.link, { responseType: 'arraybuffer' });
    //     // convert binary data to base64 encoded string
    //     const photoData = new Buffer(imageBase64Buffer.data, "binary");
    //     return { data: photoData, contentType: image.type};
    // }
}