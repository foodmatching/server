import { IngredientModel } from "../Models/Ingredient";
import { RecipeModel } from "../Models/Recipe";
import { ObjectID } from "bson";
import mongoose from "mongoose";
import { Logger } from '../Logs/WinstonLogger';
import { readFileSync } from "fs";
import * as xml2js from "xml2js";
import IngredientService from "../services/IngredientService";
import { getIngredientDataFromAPI, calculateRecipeNutrition } from "./NutritionData";
import RecipesService from "../services/RecipesService";
import { HealthIssues } from "../Models/HealthIssues";
import { UserModel } from "../Models/User";
import UsersService from "../services/UsersService";

class IngredientManager {
    // In the xml we get recipes from, we get the long ingredient name (apple. microwaved, sliced),
    // pass it to the api and get a shorter name (Apple)
    // these maps cahce the results.
    private ingredientLongNameToIngredientMap = new Map<string, any>();
    private ingredientNameToIngredientMap = new Map<string, any>();

    public async getIngredient(longName: string):Promise<IngredientModel> {
        // check long name map
        const cachedLongNameIngredient = this.ingredientLongNameToIngredientMap.get(longName);
        if (cachedLongNameIngredient) {
             return cachedLongNameIngredient
        }

        // get ingredient from API
        let ingredient: IngredientModel = await getIngredientDataFromAPI(longName);
        if (ingredient === null) {
            // ingredient not found in api! create dummy ingredient
            ingredient = {
                name: longName,
                dietaryComponents:{
                    caloricValue: 0,
                    carbohydrates: 0,
                    fats: 0,
                    sugar: 0,
                    saturatedFat: 0,
                    cholesterol : 0
                },
                goodFor: [],
                badFor: []
            } as IngredientModel;
        }

        // check short name map
        const cachedNameIngredient = this.ingredientNameToIngredientMap.get(ingredient.name);
        if (cachedNameIngredient) {
            // save in long name map
            this.ingredientLongNameToIngredientMap.set(longName, cachedNameIngredient);
            return cachedNameIngredient;
        }

        let ingredientToReturn;
        let isIngredientInDB: boolean;
        // find added ingredient if exists
        try {
            ingredientToReturn = await IngredientService.getIngredientByName(ingredient.name);
            isIngredientInDB = true;
            Logger.info(`ingredient already in db - ${ingredient.name}`);
        } catch (e) {
            isIngredientInDB = false;
        }

        // if ingredient not in db, add it
        if (!isIngredientInDB) {
            Logger.info(`adding ingredient to db - ${ingredient.name}`);
            // add ingredient to db
            const addIngredientResult = await IngredientService.add(ingredient);
            // find added ingredient
            ingredientToReturn = await IngredientService.getIngredientByName(ingredient.name);
        }

        // save to maps
        this.ingredientLongNameToIngredientMap.set(longName, ingredientToReturn);
        this.ingredientNameToIngredientMap.set(ingredient.name, ingredientToReturn);

        return ingredientToReturn as IngredientModel;
    }
}

export default class PopulateDB{

    static ingredientsIDs: ObjectID[];
    static recipeIDs: ObjectID[];
    static ingredientManager = new IngredientManager();

    public static async populate(deleteDB = false){
        Logger.info("starting populate db");

        if (deleteDB) {
            Logger.info("delete db");
            await mongoose.connection.dropDatabase();
            Logger.info("delete db finished \n");
        }

        await this.populateRecipes("src/Utils/CommonRecipes.exl");
        await this.populateRecipes("src/Utils/ArmedForcesRecipes.exl");

        Logger.info("populating db complete \n\n");
    }

    public static async populateRecipes(path: string){
        Logger.info("starting populate Recipes from " + path);

        const file = readFileSync(path, 'utf8');

        const res = await this.xmlParser(file);

        const recipes = res.data.recipe;
        for (const recipe of recipes)
        {
            const recipeName: string = recipe.$.description;
            const doesRecipeExist =  await doesRecipeExistInDB(recipeName);
            if (doesRecipeExist) {
                Logger.info("recipe exists " + recipeName);
                continue;
            }
            Logger.info("populating recipe " + recipeName);

            const recipeInstructions: string = recipe.XML_MEMO1[0];

            const ingredients = recipe.RecipeItem;
            const recipeToDb: RecipeModel = {
                name: recipeName,
                ingredients: [],
                nutritionValues: {
                    caloricValue: 0,
                    carbohydrates: 0,
                    fats: 0,
                    sugar: 0,
                    saturatedFat: 0,
                    cholesterol : 0
                },
                instructions: recipeInstructions,
                comments: [],
                type: "Common",
                // picture: { data: Buffer, contentType: String };
            } as any as RecipeModel;

            for (const ingredient of ingredients) {
                const ingredientName: string = ingredient.$.ItemName;
                const ingredientEshaId: string = ingredient.$.itemEshaCode;

                const ingredientQuantityEsha: string = ingredient.$.itemQuantity;
                const ingredientQuantity = Number(ingredientQuantityEsha);

                const ingredientMeasureKeyEsha: string = ingredient.$.itemMeasureKey;
                const ingredientMeasureKey = this.eshaMeasurementToUnit(ingredientMeasureKeyEsha);

                const ingredientData = await this.ingredientManager.getIngredient(ingredientName);

                recipeToDb.ingredients.push({
                    ingredientId: ingredientData._id,
                    amount: ingredientQuantity,
                    unit: ingredientMeasureKey
                });
            }

            //await calculateRecipeNutrition(recipeToDb);

            await RecipesService.addRecipe(recipeToDb);

            Logger.info("added recipe successfully " + recipeName);
        }

        Logger.info("populating recipes from " + path + " complete \n\n");
    }

    public static async xmlParser(s: string): Promise<any>
    {
        return new Promise(function(resolve, reject)
        {
            xml2js.parseString(s, function(err, result){
                if(err) {
                    reject(err);
                }
                else {
                    resolve(result);
                }
            });
        });
    }

    public static eshaMeasurementToUnit(x: string) : MeasurementUnit {
        /*
            1 teaspoon
            2 tablespoon
            3 cup
            5 = 43 = each / units
            6 dry serving ? also units vibe
            7 lb?
            8 gram?
            30 slices of bread
         */
        switch (x) {
            case "1":
                return MeasurementUnit.TEA_SPOON;
            case "2":
                return MeasurementUnit.TABLE_SPOON;
            case "3":
                return MeasurementUnit.CUP;
            case "7":
            case "8":
                return MeasurementUnit.MG;
            default:
                return MeasurementUnit.UNIT;
        }
    }

    public static async createDummyUsers() {
        Logger.info("creating dummy users");
        const users: UserModel[] = this.getDummyUsers();
        // Save all of the Users in the db
        for (let user of users) {
            await UsersService.add(user as UserModel);
        }
        Logger.info("finished creating dummy users");
    }

    private static getDummyUsers(): UserModel[] {
        return [{
            username: "Amit",
            password: "1234",
            email: "amit@dov.com",
            facebook: "AmitBook",
            gmail:"amit@gmail.com",
            profile: {
                name: "Amit Dov",
                gender: "female",
                hight: 170,
                wight: 55,
                whatsLookingFor: "Jahnun",
                healthIssues: [HealthIssues.LACTOSE_INTOLERANCE],
                ingredientsIDsLikes: [],
                typesLikes: ["italian"]
            },
            cookBooks: [{
                name: "amits first book",
                recipesIds: []
            },{
                name: "amits second book",
                recipesIds: []
            }]
        },{
            username: "Joe",
            password: "5678",
            email: "joe@graham.com",
            facebook: "JoeBook",
            gmail:"joe@gmail.com",
            profile: {
                name: "Joe Graham",
                gender: "male",
                hight: 182,
                wight: 75,
                whatsLookingFor: "Food",
                healthIssues: [HealthIssues.CELIAC],
                ingredientsIDsLikes: [],
                typesLikes: ["chinese"]
            },
            cookBooks: [{
                name: "joes book",
                recipesIds: []
            }]
        }] as any[];
    }
}

async function doesRecipeExistInDB(recipeName: string) {
    try {
        // throws error if doesnt exist!!
        const existingRecipe = await RecipesService.getRecipeByName(recipeName);
        return true;
    }
    catch (e) {
        return false;
    }
}
