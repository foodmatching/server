import { IngredientModel } from "../Models/Ingredient";
import { RecipeModel } from "../Models/Recipe";
import { UserModel } from "../Models/User";
import { HealthIssues , getHealthIssuesByIndex }  from "../Models/HealthIssues";
import { ObjectID } from "bson";
import mongoose from "mongoose";
import {Logger} from '../Logs/WinstonLogger';
import RecipesService from "../services/RecipesService";
import UsersService from "../services/UsersService";
import IngredientService from "../services/IngredientService";

export default class InitDB{

    static ingredientsIDs: ObjectID[];
    static recipeIDs: ObjectID[];

    public static async init(){
        Logger.info("running init db");

        // delete db
        await mongoose.connection.dropDatabase();
         
        // Creating Ingredients
        const ingredients: IngredientModel[] = InitDB.getIngredients();

        // Save all of the ingrdient in the db
        for (let ingredient of ingredients) { 
            await IngredientService.add(ingredient);
        }

        InitDB.ingredientsIDs = await IngredientService.getAllIngredientsIDs();

        const recipes: RecipeModel[] = InitDB.getRecipes();

         // Save all of the recipes in the db
        for (let recipe of recipes) {
            await RecipesService.add(recipe);
        }

        InitDB.recipeIDs = await RecipesService.getAllRecipeIds();

        const users: UserModel[] = InitDB.getUsers();
        // Save all of the Users in the db
        for (let user of users) {
            await UsersService.add(user as UserModel);
        }
    }

    private static randomIngredient(): ObjectID {
        return InitDB.ingredientsIDs[Math.floor(Math.random() * (InitDB.ingredientsIDs.length-0.01))];
    }
    private static randomRecipe(): ObjectID {
        return InitDB.recipeIDs[Math.floor(Math.random() * (InitDB.recipeIDs.length-0.01))];
    }
    private static randomAmount(): Number {
        return Math.floor(Math.random() * (3));
    }
    private static randomHealthIssue(): HealthIssues {
        const healthIssue: HealthIssues = getHealthIssuesByIndex(Math.floor(Math.random() * (4)));
        return healthIssue;
    }
    private static randomMeasurementUnit(): MeasurementUnit {
        return MeasurementUnit.G;
        // return Math.floor(Math.random() * (7)) as MeasurementUnit;
    }

    private static getIngredients(): IngredientModel[] {
        // Creating Ingredients
        return [{
                "name": "Banana",
                "dietaryComponents":{
                    "caloricValue": 80,
                    "carbohydrates": 5,
                    "fats": 6,
                    "sugar": 4,
                    "saturatedFat": 2,
                    "cholesterol" : 10
                },
                "goodFor": [InitDB.randomHealthIssue()],
                "badFor": [InitDB.randomHealthIssue()]
            },{
                "name": "Apple",
                "dietaryComponents":{
                    "caloricValue": 80,
                    "carbohydrates": 5,
                    "fats": 6,
                    "sugar": 4,
                    "saturatedFat": 2,
                    "cholesterol" : 10
                },
                "goodFor": [InitDB.randomHealthIssue()],
                "badFor": [InitDB.randomHealthIssue()]
            },{
                "name": "Cucumber",
                "dietaryComponents":{
                    "caloricValue": 80,
                    "carbohydrates": 5,
                    "fats": 6,
                    "sugar": 4,
                    "saturatedFat": 2,
                    "cholesterol" : 10
                },
                "goodFor": [],
                "badFor": [InitDB.randomHealthIssue()]
            },{
                "name": "Tomato",
                "dietaryComponents":{
                    "caloricValue": 80,
                    "carbohydrates": 5,
                    "fats": 6,
                    "sugar": 4,
                    "saturatedFat": 2,
                    "cholesterol" : 10
                },
                "goodFor": [InitDB.randomHealthIssue()],
                "badFor": []
             }
        ] as any[];
    }
    private static getRecipes(): RecipeModel[] {
    return [{
        name: "Banana pie",
        ingredients:[{
            ingredientId:InitDB.randomIngredient(),
            amount:InitDB.randomAmount(),
            unit:InitDB.randomMeasurementUnit()
        },{
            ingredientId:InitDB.randomIngredient(),
            amount:InitDB.randomAmount(),
            unit:InitDB.randomMeasurementUnit()
        },{
            ingredientId:InitDB.randomIngredient(),
            amount:InitDB.randomAmount(),
            unit:InitDB.randomMeasurementUnit()
        },{
            ingredientId:InitDB.randomIngredient(),
            amount:InitDB.randomAmount(),
            unit:InitDB.randomMeasurementUnit()
        }],
        nutritionValues:{
            caloricValue: 80,
            Carbohydrates: 5,
            Fats: 6,
            sugar: 4,
            saturatedFat: 2,
            cholesterol : 10
        } as any,
        instructions: "1.Cut the apples, the bananas and the tomato \n 2.put them in a pot and boild them",
        comments: [{
            text:"comment1",
            rank:3,
            time: new Date("2019-06-03 16:34:50.276")
        },{
            text :"comment2",
            rank:5,
            time: new Date("2019-06-03 18:34:50.276")
        }] as any[]
    },{
        name: "apple pie",
        ingredients:[{
            ingredientId:InitDB.randomIngredient(),
            amount:InitDB.randomAmount(),
            unit:InitDB.randomMeasurementUnit()
        },{
            ingredientId:InitDB.randomIngredient(),
            amount:InitDB.randomAmount(),
            unit:InitDB.randomMeasurementUnit()
        }],
        nutritionValues:{
            caloricValue: 80,
            Carbohydrates: 5,
            Fats: 6,
            sugar: 4,
            saturatedFat: 2,
            cholesterol : 10
        } as any,
        instructions: "1.Cut the apples, the bananas and the tomato \n 2.put them in a pot and boild them",
        comments: [{
            text:"comment1",
            rank:3,
            time: new Date("2019-06-03 16:33:50.276")
        },{
            text :"comment2",
            rank:5,
            time: new Date("2019-06-03 16:34:50.276")
        }] as any[]
    },{
        name: "sheperd's pie",
        ingredients:[{
            ingredientId:InitDB.randomIngredient(),
            amount:InitDB.randomAmount(),
            unit:InitDB.randomMeasurementUnit()
        },{
            ingredientId:InitDB.randomIngredient(),
            amount:InitDB.randomAmount(),
            unit:InitDB.randomMeasurementUnit()
        },{
            ingredientId:InitDB.randomIngredient(),
            amount:InitDB.randomAmount(),
            unit:InitDB.randomMeasurementUnit()
        }],
        nutritionValues:{
            caloricValue: 80,
            Carbohydrates: 5,
            Fats: 6,
            sugar: 4,
            saturatedFat: 2,
            cholesterol : 10
        } as any,
        instructions: "1.Cut the apples, the bananas and the tomato \n 2.put them in a pot and boild them",
        comments: [{
            text:"comment1",
            rank:3,
            time: new Date("2019-06-03 14:34:50.276")
        },{
            text :"comment2",
            rank:5,
            time: new Date("2019-06-03 15:34:50.276")
        }] as any[]
    }] as any[];
    }
    private static getUsers(): UserModel[] {
        return [{
            username: "Amit",
            password: "1234",
            email: "amit@dov.com",
            facebook: "AmitBook",
            gmail:"amit@gmail.com",
            profile: {
                name: "Amit Dov",
                gender: "female",
                hight: 170,
                wight: 55,
                whatsLookingFor: "Jahnun",
                healthIssues: [InitDB.randomHealthIssue()],
                ingredientsIDsLikes: [InitDB.randomIngredient(), InitDB.randomIngredient()],
                typesLikes: ["italian"]
            },
            cookBooks: [{
                name: "amits first book",
                recipes:[{
                    addedtime: new Date("2019-06-03 15:32:50.276"),
                    recipeId:InitDB.randomRecipe(),
                },{
                    addedtime: new Date("2019-06-03 15:35:50.276"),
                    recipeId:InitDB.randomRecipe(),
                }]
            },{
                name: "amits second book",
                recipes:[{
                    addedtime: new Date("2019-06-03 14:32:50.276"),
                    recipeId:InitDB.randomRecipe(),
                },{
                    addedtime: new Date("2019-06-03 16:35:50.276"),
                    recipeId:InitDB.randomRecipe(),
                }]
            }]
        },{
            username: "Joe",
            password: "5678",
            email: "joe@graham.com",
            facebook: "JoeBook",
            gmail:"joe@gmail.com",
            profile: {
                name: "Joe Graham",
                gender: "male",
                hight: 182,
                wight: 75,
                whatsLookingFor: "Food",
                healthIssues: [InitDB.randomHealthIssue()],
                ingredientsIDsLikes: [InitDB.randomIngredient(), InitDB.randomIngredient()],
                typesLikes: ["chinese"]
            },
            cookBooks: [{
                name: "joes book",
                recipes:[{
                    addedtime: new Date("2019-06-03 11:32:50.276"),
                    recipeId:InitDB.randomRecipe(),
                },{
                    addedtime: new Date("2019-06-03 12:35:50.276"),
                    recipeId:InitDB.randomRecipe(),
                },{
                    addedtime: new Date("2019-06-03 20:32:50.276"),
                    recipeId:InitDB.randomRecipe(),
                }]
            }]
        }] as any[];
    }
}
