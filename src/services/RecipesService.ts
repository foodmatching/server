// controllers/bookController.ts
import Recipe, { RecipeModel, Comment } from './../Models/Recipe'
import {Status} from "./../Models/DBInsertStatus";
import { promises, readFileSync } from 'fs';
import { add } from 'winston';
import Ingredient, { IngredientModel } from './../Models/Ingredient';
import { ObjectID } from 'bson';
import LogMessages from '../Utils/LogMessages';
import {Logger} from '../Logs/WinstonLogger';
import User from '../Models/User';
import { UserModel } from '../Models/User';
import { Document, CastError } from 'mongoose';
import { calculateRecipeNutrition } from '../Utils/NutritionData';
import ImageFinder from '../Utils/ImageFinder';
export default class RecipesService {

    private static DEFAULT_PAGE: number = 1;

    private static DEFAULT_PAGE_SIZE: number = 10;

    public static init(defaultPageSize:number){
        RecipesService.DEFAULT_PAGE_SIZE = defaultPageSize;
    }

    public static async getAllRecipes(pageNo?: number,size?:number):Promise<RecipeModel[]> {
        if(!pageNo){
            pageNo = RecipesService.DEFAULT_PAGE;
        }
        if(!size){
            size = RecipesService.DEFAULT_PAGE_SIZE;
        }
        if(pageNo < 0 || pageNo === 0) {
            throw new Error(LogMessages.ILLEGAL_PAGE_NUMBER);
        }
        const skip = size * (pageNo - 1);
        const limit = size;
        let recipes;
        try{
            recipes = await Recipe.find().skip(skip).limit(limit).exec();
            // recipes = await Recipe.find({}, '-picture -comments').skip(skip).limit(limit).exec();
        }catch(err){
            Logger.error(LogMessages.ERROR_GETING_RECIPES,err);
            throw new Error(LogMessages.ERROR_GETING_RECIPES + err);
        }
        if(recipes){
            return recipes as RecipeModel[];
        }else{
            Logger.info(LogMessages.NO_RECIPES);
            throw new Error(LogMessages.NO_RECIPES);
        }
    }
    public static async getRecipesContaining(pageNo: number,size:number,name:string){
        if(!pageNo){
          pageNo = RecipesService.DEFAULT_PAGE;
        }
        if(!size){
          size = RecipesService.DEFAULT_PAGE_SIZE;
        }
        if(pageNo < 0 || pageNo === 0) {
          throw new Error(LogMessages.ILLEGAL_PAGE_NUMBER);
        }
        const skip = size * (pageNo - 1);
        const limit = size;
        let recipes;
        try{
          recipes = await Recipe.find({name:{'$regex' : name, '$options' : 'i'}})
                                        .skip(skip)
                                        .limit(limit)
                                        .exec();
        }catch(err){
          Logger.error(LogMessages.ERROR_GETING_INGREDIENT + name,err);
          throw new Error(LogMessages.ERROR_GETING_INGREDIENT + err);
          
        }
        if(recipes.length > 0){
            return recipes;
          }else{
            throw new Error(LogMessages.RECIPE_NOT_EXIST);
          } 
      }
      
      public static async getRecipesContainingNamesAndIds(pageNo: number,size:number,name:string):Promise<{name:string, id:string}[]>{
        let recipes: Array<RecipeModel> = await this.getRecipesContaining(pageNo,size,name) as RecipeModel[];
        let recipesNames: {name:string, id:string}[] = recipes.map(e=>{
            return{ name:e.name, id:e.id}});
        return recipesNames;
        
      }
    public static async getAllRecipeIds(): Promise<ObjectID[]> {
        const recipeObjects = await Recipe.find().exec();
        const recipeIds = recipeObjects.map(r => r._id);
        return recipeIds;
    }

    public static async getRecipeByID(id: string) {
        let recipe;
        try{
            recipe = await Recipe.findById(id).exec();
        }catch(err){
            Logger.error(LogMessages.ERROR_GETING_RECIPE  + id,err);
            throw new Error(LogMessages.ERROR_GETING_RECIPE  + id + " " + err);
        }
        if(recipe){
            return recipe;
        }else{
            throw new Error(LogMessages.RECIPE_NOT_EXIST + id);
        } 
    }

    public static async getRecipeByName(name: string): Promise<RecipeModel> {
        let recipe;
        try{
            recipe = await Recipe.findOne({name: name}).exec();
        }catch(err){
            Logger.error(LogMessages.ERROR_GETING_RECIPE  + name,err);
            throw new Error(LogMessages.ERROR_GETING_RECIPE  + name + " " + err);
        }
        if(recipe){
            return recipe as RecipeModel;
        }else{
            throw new Error(LogMessages.RECIPE_NOT_EXIST + name);
        }
    }

    public static async deleteRecipeById(id: string) {
        try{
            await Recipe.deleteOne({_id :id}).exec();
            return true;
        }catch(err){
            Logger.error(LogMessages.RECIPE_NOT_EXIST+ id, err);
            throw new Error(LogMessages.RECIPE_NOT_EXIST+ id + " " + err);
        }
    }

    public static async deleteRecipeByName(name: string) {
        let recipe;
        try{
            recipe = await Recipe.findOne({name:name}).exec();
        }catch(err){
            Logger.error(LogMessages.RECIPE_NOT_EXIST + name, err);
            throw new Error(LogMessages.RECIPE_NOT_EXIST + name + " " + err);
        }
        if(!recipe){
            throw new Error(LogMessages.RECIPE_NOT_EXIST+ name);
        }else{
            try{
                await Recipe.deleteOne({name:name}).exec();
                return true;
            }catch(err){
                Logger.error(LogMessages.RECIPE_NOT_EXIST + name, err);
                throw new Error(LogMessages.RECIPE_NOT_EXIST + name + " " + err);
            }
        }
    }

    public static async addRecipe(recipe: RecipeModel){
        let returnVal = await RecipesService.add(recipe);
        if(typeof(returnVal) === "string" || (returnVal as any) instanceof String){
            return returnVal as string;
        }
        switch(returnVal){
            case Status.DataExisted:{
                throw new Error(LogMessages.RECIPE_ALREADY_EXIST);
            }
        }
        throw new Error(LogMessages.ERROR);
    }

    /**
     * addIngredients
     */
    public static async addRecipes(recipes:RecipeModel[]) {
        recipes.forEach(async (recipe: RecipeModel) => {
            try{
                await RecipesService.add(recipe);
            }catch{
                // Do nothing
            }
        });
        return true; 
    }

    /**
     * Add the given recipe to the db
     * @param givenRecipe - the recipe to add
     */
    public static async add(givenRecipe: RecipeModel): Promise<Status|string> {
        let recipe;
        try{
        // Try to get the Recipe from the DB to check it's not already saved in the DB
        recipe = await Recipe.findOne({name:  givenRecipe.name}).exec();
        }catch(err){
        Logger.error(LogMessages.RECIPE_NOT_EXIST + givenRecipe.name, err);
        throw err;
        }
        // In case its not already in the DB - save it.
        if(recipe){
            return Status.DataExisted;
        }else{
            let recipe = new Recipe(givenRecipe);
            if(givenRecipe.ingredients){
                try{
                    await calculateRecipeNutrition(recipe as RecipeModel);
                }catch(err){
                    Logger.error(LogMessages.ERROR,err);
                }  
            }
            if((recipe as RecipeModel).name){
                try{
                    (<RecipeModel>recipe).picture = await ImageFinder.searchPicture((recipe as RecipeModel).name);
                }catch(err){
                    Logger.error(LogMessages.ERROR,err);
                } 
            }
            try{
                await recipe.save();
                return (<RecipeModel> await RecipesService.getRecipeByName(givenRecipe.name)).id;
            }catch(err){
                Logger.error(LogMessages.ERROR, err);
                throw err;
            }
        }  
    } 
  
    public static async addRecipePicture(id: string, file: Express.Multer.File) {
        let recipe : RecipeModel;
        try{
            // Try to get the Recipe from the DB to check it's not already saved in the DB
            recipe = await Recipe.findById(id).exec() as RecipeModel;
        }catch(err){
            Logger.error(LogMessages.RECIPE_NOT_EXIST + id,err);
            throw new Error(LogMessages.RECIPE_NOT_EXIST + id + " " + err);
        }
        // In case its already in the DB - add the picture
        if(recipe){
            if(file){
                recipe.picture.data = readFileSync(file.path);
                recipe.picture.contentType = file.mimetype;
                try{
                    await recipe.save();
                    return true;
                }catch(err){
                    Logger.error(LogMessages.ERROR,err);
                    throw new Error(LogMessages.ERROR + " " + err);
                }
            }else{
                throw new Error(LogMessages.FILE_NOT_FOUND);
            }
        }else{
            throw new Error(LogMessages.RECIPE_NOT_EXIST + id);
        }  
    }
    public static async addRecipeCommentPicture(id: string, commentId:string, file: Express.Multer.File) {
        let recipe : RecipeModel;
        try{
            // Try to get the Recipe from the DB to check it's not already saved in the DB
            recipe = await Recipe.findById(id).exec() as RecipeModel;
        }catch(err){
            Logger.error(LogMessages.RECIPE_NOT_EXIST + id,err);
            throw new Error(LogMessages.RECIPE_NOT_EXIST + id + " " + err);
        }
        // In case its already in the DB - add the picture
        if(recipe){
            if(file){
                let comment: Comment = recipe.comments.find(e=> e.id === commentId);
                comment.picture.data = readFileSync(file.path);
                comment.picture.contentType = file.mimetype;
                try{
                    await recipe.save();
                    return true;
                }catch(err){
                    Logger.error(LogMessages.ERROR,err);
                    throw new Error(LogMessages.RECIPE_NOT_EXIST + id + " " + err);
                }
            }else{
                throw new Error(LogMessages.FILE_NOT_FOUND);
            }
        }else{
            throw new Error(LogMessages.RECIPE_NOT_EXIST)
        }  
    }

    public static async addCommentToRecipe(id: string, comment:Comment){
        let recipe : RecipeModel;
        try{
            // Try to get the Recipe from the DB to check it's not already saved in the DB
            recipe = await Recipe.findById(id).exec() as RecipeModel;
        }catch(err){
            Logger.error(LogMessages.RECIPE_NOT_EXIST + id, err);
            throw new Error(LogMessages.RECIPE_NOT_EXIST + id + " " + err);
        }
        // In case its already in the DB - save it.
        if(recipe){
            comment.time = new Date();
            if(!comment.authorName){
                const user = ((await User.findById(comment.userID).exec()) as UserModel);
                comment.authorName = user.username;
            }
            recipe.comments.push(comment);
            recipe = await RecipesService.recalculateRank(recipe);
            try{
                await recipe.save();
            }catch(err){
                Logger.error(LogMessages.ERROR, err);
                throw new Error(LogMessages.ERROR + err);
            }
            recipe = await Recipe.findById(id).exec() as RecipeModel;
            let commentFromDB = recipe.comments.filter(e=> {
                if((e.time) && (e.time.getTime() === comment.time.getTime())){
                return true;  
                }
                return false;
            });
            return commentFromDB[0]._id;
        }else{
            throw new Error(LogMessages.RECIPE_NOT_EXIST + id);
        }  
    }
  
    public static async getAllCommentsForRecipe(id:string){
        let recipe: RecipeModel;
        try{
        // Try to get the Recipe from the DB
        recipe = await Recipe.findById(id).exec() as RecipeModel;
        }catch(err){
        Logger.error(LogMessages.RECIPE_NOT_EXIST + id, err);
        throw new Error(LogMessages.RECIPE_NOT_EXIST + id + " " + err);
        }
        // In case its exist in the db
        if(recipe){
        return recipe.comments;
        }
    }

    private static recalculateRank(givenRecipe: RecipeModel): RecipeModel{
        let count: number = 0;
        let rankSum: number = 0;
        givenRecipe.comments.forEach((comment: Comment) => {
        if(comment.rank){
            count++;
            rankSum+=comment.rank;
        }
        });
        givenRecipe.rank = rankSum / count;
        return givenRecipe;
    }

    public static async getIngredients(id:String): Promise<Array<IngredientModel>>{
        let recipe: RecipeModel;
        // Try to get the Recipe from the DB
        try{
            recipe  = await Recipe.findById(id).exec() as RecipeModel;
        }catch(err){
            if(err instanceof CastError){
                throw new Error(LogMessages.RECIPE_NOT_EXIST + id);
            }else{
                Logger.error(LogMessages.RECIPE_NOT_EXIST + id,err);
                throw new Error(LogMessages.RECIPE_NOT_EXIST + id +err); 
            }    
        }
    
        if(!recipe){
            throw new Error(LogMessages.RECIPE_NOT_EXIST + id);
        }
        try{
            let Ingredients = await Ingredient.find({_id:{
                $in: (recipe.ingredients.map(e=> e.ingredientId))}},
                '-picture -dietaryComponents')
                .exec();
                return Ingredients as Array<IngredientModel>;
        }catch(err){
            Logger.error(LogMessages.RECIPE_NOT_EXIST + id,err);
            throw new Error(LogMessages.ERROR + id + " " + err);
        }  
    }
}