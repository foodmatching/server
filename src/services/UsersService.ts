import Recipe, { RecipeModel } from '../Models/Recipe'
import User, { UserModel } from '../Models/User';
import {Status} from "../Models/DBInsertStatus";
import { readFileSync } from 'fs';
import CookBook from '../Models/Cookbook';
import Ingredient, { IngredientModel } from '../Models/Ingredient';
import { HealthIssues } from '../Models/HealthIssues';
import { ObjectID } from 'bson';
import LogMessages from '../Utils/LogMessages';
import {Logger} from '../Logs/WinstonLogger';
import RecipesService from './RecipesService';
import RecipeInCookBook from '../Models/RecipeInCookBook';
import RecipeInCookBookOutput from '../Models/RecipeInCookBookOutput';

export default class UsersService{

  private static DEFAULT_PAGE: number = 1;

  private static DEFAULT_PAGE_SIZE: number = 10;

  public static init(defaultPageSize:number){
    UsersService.DEFAULT_PAGE_SIZE = defaultPageSize;
  }

  /**
  * get All Users
  */
  public static async getAllUsers(pageNo: number,size:number) {
   
    if(!pageNo){
        pageNo = UsersService.DEFAULT_PAGE;
    }
    if(!size){
        size = UsersService.DEFAULT_PAGE_SIZE;
    }
            
    if(pageNo < 0 || pageNo === 0) {
        throw new Error(LogMessages.ILLEGAL_PAGE_NUMBER);
    }
    const skip = size * (pageNo - 1);
    const limit = size;
    let users;
    try{
        users = await User.find().limit(limit).skip(skip).exec();
    }catch(err){
        Logger.error(LogMessages.ERROR,err);
        throw new Error(LogMessages.ERROR + err);
    }
    if(users){
        return users;
    }else{
        throw new Error(LogMessages.NO_USERS);
    }
    
  }

  /**
   * Get the specific user by it's ID
   * @param req 
   * @param res 
   */
   public static async getUserByID(id: string) {
       let user;
        try{
            user = await User.findById(id).exec();
        }catch(err){
            Logger.error(LogMessages.USER_NOT_EXIST + id, err);
            throw new Error(LogMessages.USER_NOT_EXIST + id + " " + err);
        }
        if(user){
            return user;
        }else{
            throw new Error(LogMessages.USER_NOT_EXIST + id);
        } 
    }
    
  public static async getUserNameAndPictureByID(id: string) {
    let user;
     try{
         user = await User.findById(id,'picture profile.name').exec();
     }catch(err){
         Logger.error(LogMessages.USER_NOT_EXIST + id, err);
         throw new Error(LogMessages.USER_NOT_EXIST + id + " " + err);
     }
     if(user){
         return user;
     }else{
         throw new Error(LogMessages.USER_NOT_EXIST + id);
     } 
 }
    public static async getUserByUserName(username:string) {
        let user;
        try{
            user = await User.findOne({username: username}).exec();
        }catch(err){
            Logger.error(LogMessages.USER_NOT_EXIST + username, err);
            throw new Error(LogMessages.USER_NOT_EXIST + username + " " + err);
        }
        if(user){
            return user;
        }else{
            throw new Error(LogMessages.USER_NOT_EXIST + username);
        }
        
    }
   
    public static async getUserByGmail(gmail: string) {
        let user;
        try{
            user = await User.findOne({gmail: gmail}).exec();
        }catch(err){
            Logger.error(LogMessages.USER_NOT_EXIST + gmail, err);
            throw new Error(LogMessages.USER_NOT_EXIST + gmail + " " + err);   
        }
        if(user){
            return user;
        }else{
            throw new Error(LogMessages.USER_NOT_EXIST + gmail);
        }   
    }
    
    public static async getUserByFacebook(facebook: string) {
        let user;
        try{
            user = await User.findOne({facebook: facebook}).exec();
        }catch(err){
            Logger.error(LogMessages.USER_NOT_EXIST + facebook,  err);
            throw new Error(LogMessages.USER_NOT_EXIST + facebook + " " + err); 
        }
        if(user){
           return user;
        }else{
           throw new Error(LogMessages.USER_NOT_EXIST + facebook);
        }
    }

    public static async deleteUserById(id:string) {
        try{
            await User.deleteOne({_id :id}).exec();
            return true;
        }catch(err){
            Logger.error(LogMessages.USER_NOT_EXIST + id, err);
            throw new Error(LogMessages.USER_NOT_EXIST + id + " " + err);
        }
    }

    public static async deleteUserByUserName(username:string) {
        let user;
        try{
             user = await User.findOne({username:username}).exec();
        }catch(err){
            Logger.error(LogMessages.USER_NOT_EXIST + username, err);
            throw new Error(LogMessages.USER_NOT_EXIST + username + " " + err);
        }
        if(!user){
            throw new Error(LogMessages.USER_NOT_EXIST + username);
        }else{
            try{
                await User.deleteOne({username:username}).exec();
            }catch(err){
                Logger.error(LogMessages.CANT_DELETE + username, err);
                throw new Error(LogMessages.CANT_DELETE + username + " " + err);
            }    
            return true;
        }
            
    }

    public static async connect(identifier:string,password:string) {
        let user: UserModel;
        try{
            if(identifier){
                user = await User.findOne({username:identifier}).exec() as UserModel;
            }
            if(!user){
                user = await User.findOne({facebook: identifier}).exec() as UserModel;
                if(!user){
                    user = await User.findOne({gmail: identifier}).exec() as UserModel;
                    if(!user){
                        user = await User.findOne({email: identifier}).exec() as UserModel;
                    }
                }
            }
        }catch(err){
            if(identifier){
                Logger.error(LogMessages.USER_NOT_EXIST + identifier, err);
                throw new Error(LogMessages.USER_NOT_EXIST + identifier + " " + err);
            }
        }
        if(user){
            if((user as UserModel).password === password){
                return true;
            }else{
                throw new Error(LogMessages.INCORRECT_PASSWORD);
            }
        }else{
            throw new Error(LogMessages.NO_DATA);
        }   
    }
   
    public static async addNewUser(user: UserModel){
        let status: Status
        try{
            status = await UsersService.add(user);
        }catch(err){
            Logger.error(LogMessages.ERROR,err);
            throw err;
        }
        switch(status){
            case Status.DataExisted:{
                throw new Error(LogMessages.USER_ALREADY_EXIST);
                break;
            }case Status.seccseed:{
                return true;
                break;
            }case Status.NoDataToSave:{
                throw new Error(LogMessages.NO_DATA);
            }
        }
    }

    public static async addNewUsers(users: UserModel[]) {
        users.forEach(async (user: UserModel) => {
            await UsersService.add(user);
        });
       return true; 
    }

    /**
     * Add the given user to the db
     * @param givenUser - the user to add
     */
    public static async add(givenUser: UserModel): Promise<Status> {
        let founduser: UserModel;
        try{
            if(givenUser.username){
                founduser = await User.findOne({username:givenUser.username}).exec() as UserModel;
            }
            if((givenUser.facebook) &&(!founduser)){
                founduser = await User.findOne({facebook: givenUser.facebook}).exec() as UserModel;
            }if((givenUser.gmail)&&(!founduser)){
                founduser = await User.findOne({gmail: givenUser.gmail}).exec() as UserModel;
            }if((givenUser.email)&&(!founduser)){
                founduser = await User.findOne({email: givenUser.email}).exec() as UserModel;
            }else{
                return Status.NoDataToSave;
            }
        }catch(err){
            if(givenUser.username){
                Logger.error(LogMessages.USER_NOT_EXIST + givenUser.username, err);
            }
            else if(givenUser.facebook){
                Logger.error(LogMessages.USER_NOT_EXIST + givenUser.facebook, err);
            }else if(givenUser.gmail){
                Logger.error(LogMessages.USER_NOT_EXIST + givenUser.gmail, err);
            }else if(givenUser.email){
                Logger.error(LogMessages.USER_NOT_EXIST + givenUser.email, err);
            }
           
            return Status.error;
        }
        // In case its not already in the DB - save it.
        if(founduser){
            return Status.DataExisted;
        }
    
        let user = new User(givenUser);
        try{
            await user.save()
        }catch(err){
            Logger.error(LogMessages.ERROR, err);
            throw err;
        }
        return Status.seccseed;  
    } 

    /**
     * addHelthIssues
     */
    public static async addHelthIssues(id: string, healthIssues:HealthIssues[]|HealthIssues) {
        const user: UserModel = await User.findById(id).exec() as UserModel;
        if(user){
            if(healthIssues){
                if(user.profile.healthIssues.find(e=>(e ===healthIssues))){
                    throw new Error(LogMessages.HEALTH_ISSUE_ALLREADY_MARKED);
                }if(Array.isArray(healthIssues)){
                    let intersect: HealthIssues[];
                    (healthIssues as Array<HealthIssues>).forEach(e=>{
                        if(user.profile.healthIssues.find(hi=>(hi===e))){
                            intersect = intersect.concat(e);
                        }
                    })
                    healthIssues = healthIssues.filter(e=>(intersect.find(x=> x===e) === undefined))
                }
                   
                user.profile.healthIssues = user.profile.healthIssues.concat(healthIssues);
                user.save();
                return true;
            }else{
                throw new Error(LogMessages.HEALTH_ISSUE_EMPTY);
            } 
        }else{
            throw new Error(LogMessages.USER_NOT_EXIST + id);
        }
    }

     /**
     * add ingredients ids likes
     */
    public static async addIngredientsIDsLikes(id: string, ingredientsIDsLikes:ObjectID[]|ObjectID) {
        const user : UserModel= await User.findById(id).exec() as UserModel;
        if(user){
            if(ingredientsIDsLikes){
                if(user.profile.ingredientsIDsLikes.find(e=>(e ===ingredientsIDsLikes))){
                    throw new Error(LogMessages.INGREDIENT_ALLREADY_LIKED);
                }
                if(Array.isArray(ingredientsIDsLikes)){
                    let intersect: ObjectID[];
                    (ingredientsIDsLikes as Array<ObjectID>).forEach(e=>{
                       if(user.profile.ingredientsIDsLikes.find(hi=>(hi===e))){
                            intersect = intersect.concat(e);
                        }
                    })
                    ingredientsIDsLikes = ingredientsIDsLikes.filter(e=>(intersect.find(x=> x===e) === undefined))
                    
                }
                user.profile.ingredientsIDsLikes = user.profile.ingredientsIDsLikes.concat(ingredientsIDsLikes);
                user.save();
                return true;
                
            }else{
                throw new Error(LogMessages.INGREDIENT_LIKED_EMPTY);
            }   
        }else{
            throw new Error(LogMessages.USER_NOT_EXIST + id);
        }
    }

    /**
     * add types likes
     */
    public static async addTypesLikes(id: string, typesLikes:string[]|string) {
        const user: UserModel = await User.findById(id).exec() as UserModel;
        if(user){
            if(typesLikes){
                if(user.profile.typesLikes.find(e=>(e ===typesLikes))){
                    throw new Error(LogMessages.TYPE_ALLREADY_LIKED);
                }
                if(Array.isArray(typesLikes)){
                    let intersect: string[];
                    (typesLikes as Array<string>).forEach(e=>{
                       if(user.profile.typesLikes.find(hi=>(hi===e))){
                            intersect = intersect.concat(e);
                        }
                    })
                    typesLikes = typesLikes.filter(e=>(intersect.find(x=> x===e) === undefined))
                    
                }
                    user.profile.typesLikes = user.profile.typesLikes.concat(typesLikes);
                    user.save();
                    return true;
                
            }else{
                throw new Error(LogMessages.TYPE_LIKED_EMPTY);
            } 
        }else{
            throw new Error(LogMessages.USER_NOT_EXIST + id);
        }
    }

    public static async addUserPicture(id:string, file: Express.Multer.File) {
        const user: UserModel = await User.findById(id).exec() as UserModel;
        if(user){
            if(file){
                user.profile.picture.data = readFileSync(file.path);
                user.profile.picture.contentType = file.mimetype;

                user.save();
                return true;
            }else{
                throw new Error(LogMessages.FILE_NOT_FOUND);
            } 
        }else{
            throw new Error(LogMessages.USER_NOT_EXIST + id);
        }
    }

    public static async addCookBook(id:string, name:string) {
        const user: UserModel = await User.findById(id).exec() as UserModel;
        if(user){
            let cookbooks = user.cookBooks;
            cookbooks.forEach((cookBook)=>{
                if(cookBook.name === name){
                    throw new Error(LogMessages.COOKBOOK_ALLREADY_EXIST);
                }
            });
            cookbooks.push(new CookBook(name));
            user.save();
            return true;
        }else{
            throw new Error(LogMessages.USER_NOT_EXIST + id);
        }
    }

    public static async deleteCookBook(id:string, cookBookName:string, cookBookId?:string) {
        const user: UserModel = await User.findById(id).exec() as UserModel;
        if(user){
            let cookbooks: CookBook[] = user.cookBooks;
            if(cookBookName){
                cookbooks = cookbooks.filter(e=>{
                    if(e.name === cookBookName){
                        return false;
                    }
                    return true;
                });
            }else if(cookBookId){
                cookbooks = cookbooks.filter(e=>{
                    if((e as any).id === cookBookId){
                        return false;
                    }
                    return true;
                });
            }
            user.cookBooks = cookbooks;
            user.save();
            return true;
        }else{
            throw new Error(LogMessages.USER_NOT_EXIST + id);
        }
    }
    public static async addRecipeToCookBook(id:string,name:string,recipeId:ObjectID) {
        const user : UserModel = await User.findById(id).exec() as UserModel;
        if(user){

            let cookbooks = user.cookBooks;
            let cookBook =cookbooks.find(e=>(e.name === name));
            
            if(cookBook.recipes.find(e=>(e.recipeId===recipeId))){
                throw new Error(LogMessages.RECIPE_ALLREADY_IN_COOKBOOK);
            }else{
                let recipeToAdd: RecipeInCookBook = new RecipeInCookBook(recipeId);
                cookBook.recipes.push(recipeToAdd);
                user.save();
                return true;
            }
        }else{
            throw new Error(LogMessages.USER_NOT_EXIST + id);
        }
    }

    public static async removeRecipeFromCookBook(id:string,cookBookName:string,recipeId:ObjectID) {
        const user : UserModel = await User.findById(id).exec() as UserModel;
        if(user){
            let cookbooks = user.cookBooks;
            let cookBook;
            if(cookBookName){
                cookBook = cookbooks.find(e=>(e.name === cookBookName));
            }else{
                throw new Error(LogMessages.NO_DATA);
            }
            if(!cookBook){
                throw new Error(LogMessages.COOKBOOK_NOT_EXIST);
            }
            cookBook = cookBook.recipes.filter(e=>{
                if(e.recipeId === recipeId){
                    return false;
                }
                return true;
            })
            user.save();
            return true;
        }else{
            throw new Error(LogMessages.USER_NOT_EXIST + id);
        }
    }
   
    public static async getRecpiesMatchForUser(id: string, pageNo: number, size: number): Promise<RecipeModel[]> {
        Logger.info("started getRecpiesMatchForUser");
        const user = await User.findById(id).exec() as UserModel;
        if (!user) {
            Logger.info("finished getRecpiesMatchForUser - user not found");
            throw new Error(LogMessages.USER_NOT_EXIST + id);
        }
        const userIngredients: ObjectID[] = user.profile.ingredientsIDsLikes;
        // if (userIngredients.length == 0) {
        //     throw new Error(LogMessages.USER_DONT_LIKES_INGREDIENTS);
        //     return;
        // }
        const userTypes: string[] = user.profile.typesLikes;
        if (userTypes.length == 0) {
            throw new Error(LogMessages.USER_DONT_LIKES_TYPES);
        }
        const userRecipes: ObjectID[] = UsersService.getAllUserRecipesIDs(user);
        if(!pageNo){
            pageNo = UsersService.DEFAULT_PAGE;
        }
        if(!size){
            size = UsersService.DEFAULT_PAGE_SIZE;
        }
        
        if(pageNo < 0 || pageNo === 0) {
            throw new Error(LogMessages.ILLEGAL_PAGE_NUMBER);
        }
        const skip = size * (pageNo - 1);
        const limit = size;
        let recipes;
        try{
            recipes = await Recipe
                .aggregate([
                    // possible perfirnance enhancer:
                    // {
                    // '$project': {
                    //     'picture': 0,
                    //     'comments': 0
                    // }
                    // },
                    {
                    '$match': {
                        '_id': {
                            '$nin': userRecipes
                        },
                        'ingredients': {
                            '$not': {
                              '$size': 0
                            }
                        }
                    }
                    },
                    {
                        '$addFields': {
                        'typematches': {
                            '$cond': {
                            'if': {
                                '$in': [
                                '$type', userTypes
                                ]
                            }, 
                            'then': 1, 
                            'else': 0
                            }
                        },
                        'ingredientsMatch': {
                            '$divide': [
                            {
                                '$size': {
                                '$filter': {
                                    'input': '$ingredients', 
                                    'as': 'i', 
                                    'cond': {
                                    '$in': [
                                        '$$i.ingredientId', userIngredients
                                    ]
                                    }
                                }
                                }
                            }, {
                                '$size': '$ingredients'
                            }
                            ]
                        }
                        }
                    }, {
                        '$addFields': {
                        'priority': {
                            '$add': [
                            {
                                '$multiply': [
                                0.7, '$ingredientsMatch'
                                ]
                            }, {
                                '$multiply': [
                                0.3, '$typematches'
                                ]
                            }
                            ]
                        }
                        }
                    }, {
                        '$sort': {
                        'priority': -1
                        }
                    }, {
                        '$skip': skip
                    }, {
                        '$limit': limit
                    }
                    ]);
        }catch(err){
            Logger.error(LogMessages.ERROR,err);
            throw new Error(LogMessages.ERROR + err);
        }
        Logger.info("finished getRecpiesMatchForUser");
        // fix pictures
        recipes.forEach(r => {if (r.picture) r.picture.data = r.picture.data.buffer});
        return recipes as RecipeModel[];
    }

    public static async getAllCookbooks(id: string){
        const user: UserModel = await User.findById(id).exec() as UserModel;
        if(user){
            return user.cookBooks;
        }else{
            throw new Error(LogMessages.USER_NOT_EXIST + id);
        }
    }

    private static getAllUserRecipesIDs(user: UserModel): Array<ObjectID>{
        let userRecipesID: ObjectID[]  = new Array<ObjectID>();
        if(user){
            user.cookBooks.forEach(cookBook=>{
                userRecipesID.concat(cookBook.recipes.map(e=> e.recipeId))
            });
        }
        return userRecipesID;
    }
    
    public static async getAllRecipesInCookBook(id:string, name:string) {
        const user: UserModel = await User.findById(id).exec() as UserModel;
        if(user){
            let cookbooks = user.cookBooks;
            let cookBook: CookBook = cookbooks.find(e=>(e.name === name));
            let recipes = await UsersService.getRecipes(cookBook);
            return recipes;
        }else{
            throw new Error(LogMessages.USER_NOT_EXIST + id);
        }
    }

    private static async getRecipes(cookBook: CookBook): Promise<Array<RecipeInCookBookOutput>>{
        const recipes = new Array<RecipeInCookBookOutput>();
        for (const recipe of cookBook.recipes) {
            recipes.push(await RecipeInCookBookOutput.build(recipe));
        }
        
        return recipes.sort(RecipeInCookBookOutput.sort);
    }
    public static async getAllUserRecipes(id: string): Promise<Array<RecipeInCookBookOutput>>{
        const user: UserModel = await User.findById(id).exec() as UserModel;
        const recipes = new Array<RecipeInCookBookOutput>();
        for( const cookBook of user.cookBooks){
            for (const recipe of cookBook.recipes) {
                recipes.push(await RecipeInCookBookOutput.build(recipe));
            }
        }
        return recipes.sort(RecipeInCookBookOutput.sort);
    }
    
    public static async isRecipeRecommendedForUser(userId:string,recipeId:string){
        const user: UserModel = await User.findById(userId).exec() as UserModel;
        if(!user){
            throw new Error(LogMessages.USER_NOT_EXIST + userId);
        }
        const recipe = await Recipe.findById(recipeId).exec();
        if(recipe){
            let ingredients = await RecipesService.getIngredients(recipeId);
            let badFor: HealthIssues[] = new Array();
            ingredients.forEach(ingredient=>{
                badFor = badFor.concat(ingredient.badFor.filter(issue=>(user.profile.healthIssues.indexOf(issue) != -1)));
            });
            if(badFor.length > 0){
                return RecommendedLevel.NOT_RECOMANDED;
            }else{
                let goodFor: HealthIssues[] = new Array();
                ingredients.forEach(ingredient=>{
                    goodFor = goodFor.concat(ingredient.badFor.filter(issue=>(user.profile.healthIssues.indexOf(issue) != -1)));
                });
                if(goodFor.length > 0){
                    return RecommendedLevel.RECOMANDED;
                }else{
                    return RecommendedLevel.OK;
                }
            }
        }else{
            throw new Error(LogMessages.RECIPE_NOT_EXIST + recipeId);
        }
    }

    public static async isIngredientRecommendedForUser(userId:string,ingredientId:string){
        const user : UserModel = await User.findById(userId).exec() as UserModel;
        if(!user){
            throw new Error(LogMessages.USER_NOT_EXIST + userId);
        }
        const ingredient : IngredientModel = await Ingredient.findById(ingredientId).exec() as IngredientModel;
        if(ingredient){
            let badFor: HealthIssues[] = ingredient.badFor.filter(issue=>(user.profile.healthIssues.indexOf(issue) != -1));
            
            if(badFor.length > 0){
                return RecommendedLevel.NOT_RECOMANDED;
            }else{
                let goodFor: HealthIssues[] =ingredient.badFor.filter(issue=>(user.profile.healthIssues.indexOf(issue) != -1));
                if(goodFor.length > 0){
                    return RecommendedLevel.RECOMANDED;
                }else{
                    return RecommendedLevel.OK;
                }
            }
        }else{
            throw new Error(LogMessages.INGREDIENT_NOT_EXIST + ingredientId);
        }
    }
}