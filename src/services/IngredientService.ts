// controllers/bookController.ts
import {Request, Response} from 'express'
import Ingredient, {IngredientModel} from '../Models/Ingredient'
import {Status} from "../Models/DBInsertStatus";
import {ObjectID} from 'bson';
import {readFileSync} from 'fs';
import LogMessages from '../Utils/LogMessages';
import { Logger } from '../Logs/WinstonLogger';
import { HealthIssues } from '../Models/HealthIssues';
import { getIngredientDataFromAPI } from "../Utils/NutritionData";
export default class IngredientService {

  private static DEFAULT_PAGE: number = 1;

  private static DEFAULT_PAGE_SIZE: number = 10;

  public static init(defaultPageSize:number){
    IngredientService.DEFAULT_PAGE_SIZE = defaultPageSize;
  }
  
  public static async getAllIngredients(pageNo: number,size:number) {
    if(!pageNo){
      pageNo = IngredientService.DEFAULT_PAGE;
    }
    if(!size){
      size = IngredientService.DEFAULT_PAGE_SIZE;
    }
    if(pageNo < 0 || pageNo === 0) {
      throw new Error(LogMessages.ILLEGAL_PAGE_NUMBER);
    }
      
    const skip = size * (pageNo - 1);
    const limit = size;
    let ingredients;
    try{
      ingredients = await Ingredient.find().skip(skip).limit(limit).exec();
    }catch(err){
      Logger.error(LogMessages.ERROR_GETING_INGREDIENTS,err);
      throw new Error(LogMessages.ERROR_GETING_INGREDIENTS + err);
    }
    if(ingredients){
        return ingredients;
    }else{
      Logger.info(LogMessages.NO_INGREDIENTS);
      throw new Error(LogMessages.NO_RECIPES);
    }
  }

  public static async getAllIngredientsIDs(): Promise<Array<ObjectID>>{
    const ingredientsIDObjects: any[] = await Ingredient.find().exec();
    const ingredientIDs = ingredientsIDObjects.map(ingredientObject => ingredientObject._id);
    return ingredientIDs;
  }
  
  public static async getIngredientByID(id: string): Promise<IngredientModel> {
    let ingredient;
    try{
        ingredient = await Ingredient.findById(id).exec();
      }catch(err){
        Logger.error(LogMessages.INGREDIENT_NOT_EXIST + id,err);
        throw new Error(LogMessages.ERROR_GETING_INGREDIENT + err);
      }
        if(ingredient){
          return ingredient as IngredientModel;
        }else{
          throw new Error(LogMessages.INGREDIENT_NOT_EXIST + id);
        }
  }

  public static async getIngredientByObjectID(id: ObjectID): Promise<IngredientModel> {
    let ingredient;
    try{
      ingredient = await Ingredient.findById(id).exec();
    }catch(err){
      Logger.error(LogMessages.INGREDIENT_NOT_EXIST + id,err);
      throw new Error(LogMessages.ERROR_GETING_INGREDIENT + err);
    }
    if(ingredient){
      return ingredient as IngredientModel;
    }else{
      throw new Error(LogMessages.INGREDIENT_NOT_EXIST + id);
    }
  }

  public static async getIngredientByName(name: string) {
    let ingredient;
    try{
        ingredient = await Ingredient.findOne({name: name}).exec();
    }catch(err){
      Logger.error(LogMessages.ERROR_GETING_INGREDIENT + name,err);
      throw new Error(LogMessages.ERROR_GETING_INGREDIENT + err);
    }
    if(ingredient){
      return ingredient;
    }else{
      throw new Error(LogMessages.INGREDIENT_NOT_EXIST);
    } 
  }

  public static async getIngredientStartWith(pageNo: number,size:number,name:string){
    if(!pageNo){
      pageNo = IngredientService.DEFAULT_PAGE;
    }
    if(!size){
      size = IngredientService.DEFAULT_PAGE_SIZE;
    }
    if(pageNo < 0 || pageNo === 0) {
      throw new Error(LogMessages.ILLEGAL_PAGE_NUMBER);
    }
    const skip = size * (pageNo - 1);
    const limit = size;
    let ingredients;
    try{
      ingredients = await Ingredient.find({name:{'$regex' : "^" +name, '$options' : 'i'}},'-picture -dietaryComponents -goodFor -badFor')
                                    .skip(skip)
                                    .limit(limit)
                                    .exec();
    }catch(err){
      Logger.error(LogMessages.ERROR_GETING_INGREDIENT + name,err);
      throw new Error(LogMessages.ERROR_GETING_INGREDIENT + err);
      
    }
    if(ingredients.length > 0){
        return ingredients;
      }else{
        throw new Error(LogMessages.INGREDIENT_NOT_EXIST);
      } 
  }

  public static async deleteIngredientById(id: string) {
    try{
      await Ingredient.deleteOne({_id : id}).exec();
      return true;
    }catch(err){
      Logger.error(LogMessages.INGREDIENT_NOT_EXIST + id,err);
      throw new Error(LogMessages.INGREDIENT_NOT_EXIST + id + " " + err);
    }
  }

  /**
   * getSpesificIngredient
   */
  public static async deleteIngredientByName(name: string) {
    let ingredient;
    try{
      ingredient = await Ingredient.findOne({name:name}).exec();
    }catch(err){
      Logger.error(LogMessages.ERROR_GETING_INGREDIENT+ name,err);
      throw new Error(LogMessages.ERROR_GETING_INGREDIENT+ name + " " + err);
    }
    if(!ingredient){
      throw new Error(LogMessages.INGREDIENT_NOT_EXIST + name);
    }else{
      try{
        await Ingredient.deleteOne({name:name}).exec();
        return true;
      }catch(err){
        Logger.error(LogMessages.INGREDIENT_NOT_EXIST + name, err);
        throw new Error(LogMessages.INGREDIENT_NOT_EXIST + name + " " + err);
      }
    }
    
  }

  public static  async addIngredient(ingredient: IngredientModel){
    if(!ingredient){
      throw new Error(LogMessages.NO_DATA);
    }
    let status: Status = await IngredientService.add(ingredient);
    
    switch(status){
      case Status.DataExisted:{
        throw new Error(LogMessages.INGREDIENT_ALREADY_EXIST); 
      }case Status.seccseed:{
        return true;
      }
    }
  }

  public static async addIngredients(ingredients: IngredientModel[]) {
    ingredients.forEach(async (ingredient: IngredientModel) => {
      try{
      await IngredientService.add(ingredient);
      }catch{
        //do nothing
      }
    });
    return true; 
  }

  /**
   * Add the given ingredient to the db
   * @param givenIngredient - the ingredient to add
   */
  public static async add(givenIngredient: IngredientModel): Promise<Status> {
    let ingredient;
    try{
      // Try to get the Ingredient from the DB to check it's not already saved in the DB
      ingredient = await Ingredient.findOne({name:  givenIngredient.name}).exec();
    }catch(err){
      Logger.error(LogMessages.INGREDIENT_NOT_EXIST + givenIngredient.name,err);
      throw err;
    }
    // In case its not already in the DB - save it.
    if(ingredient){
      return Status.DataExisted;
    }
       // get ingredient from API
      let ingredientFromApi: IngredientModel = await getIngredientDataFromAPI(givenIngredient.name);
      if (ingredientFromApi === null) {
           // ingredient not found in api! create dummy ingredient
           ingredientFromApi = {
               name: givenIngredient.name,
               dietaryComponents:{
                   caloricValue: 0,
                   carbohydrates: 0,
                   fats: 0,
                   sugar: 0,
                   saturatedFat: 0,
                   cholesterol : 0
               },
               goodFor: givenIngredient.goodFor? givenIngredient.goodFor: [],
               badFor:  givenIngredient.badFor? givenIngredient.badFor: [],
           } as IngredientModel;
        }else{
          if(!ingredientFromApi.name){
            ingredientFromApi.name = givenIngredient.name;
          }else{
            ingredient = await Ingredient.findOne({name:  ingredientFromApi.name}).exec();
             // In case its not already in the DB - save it.
            if(ingredient){
                return Status.DataExisted;
              }
          }
          ingredientFromApi.goodFor = givenIngredient.goodFor ? givenIngredient.goodFor: [];
          ingredientFromApi.badFor = givenIngredient.badFor ? givenIngredient.badFor: [];
        }
      let finalIngredient = new Ingredient(ingredientFromApi);
      try{
        await finalIngredient.save()
      }catch(err){
        Logger.error(LogMessages.ERROR,err);
        throw err;
      }
      return Status.seccseed;  
  } 

  public static async addBadFor(id:string, healthIssues:HealthIssues[]|HealthIssues) {
    let ingredient: import("mongoose").Document | IngredientModel;
    try{
      // Try to get the Ingredient from the DB to check it's not already saved in the DB
      ingredient = await Ingredient.findById(id).exec();
    }catch(err){
      Logger.error(LogMessages.INGREDIENT_NOT_EXIST +  id,err);
      throw new Error(LogMessages.INGREDIENT_NOT_EXIST + id);
    }
    if(ingredient){
      if(healthIssues){
        if((ingredient as IngredientModel).badFor.find(e=>(e ===healthIssues))){
            throw new Error(LogMessages.HEALTH_ISSUE_ALLREADY_MARKED);
        }if(Array.isArray(healthIssues)){
            let intersect: HealthIssues[];
            (healthIssues as Array<HealthIssues>).forEach(e=>{
                if((ingredient as IngredientModel).badFor.find(hi=>(hi===e))){
                    intersect = intersect.concat(e);
                }
            })
            healthIssues = healthIssues.filter(e=>(intersect.find(x=> x===e) === undefined))
        }
      (ingredient as IngredientModel).badFor = (ingredient as IngredientModel).badFor.concat(healthIssues); 
      ingredient.save();
      return true;
      }else{
        throw new Error(LogMessages.HEALTH_ISSUE_EMPTY);
      }
    }else{
      throw new Error(LogMessages.INGREDIENT_NOT_EXIST + id);
    }
  }

  public static async addGoodFor(id:string, healthIssues:HealthIssues[]|HealthIssues) {
    let ingredient: import("mongoose").Document | IngredientModel;
    try{
      // Try to get the Ingredient from the DB to check it's not already saved in the DB
      ingredient = await Ingredient.findById(id).exec();
    }catch(err){
      Logger.error(LogMessages.INGREDIENT_NOT_EXIST +  id,err);
      throw new Error(LogMessages.INGREDIENT_NOT_EXIST + id);
    }
    if(ingredient){
      if(healthIssues){
        if((ingredient as IngredientModel).goodFor.find(e=>(e ===healthIssues))){
            throw new Error(LogMessages.HEALTH_ISSUE_ALLREADY_MARKED);
        }if(Array.isArray(healthIssues)){
            let intersect: HealthIssues[];
            (healthIssues as Array<HealthIssues>).forEach(e=>{
                if((ingredient as IngredientModel).goodFor.find(hi=>(hi===e))){
                    intersect = intersect.concat(e);
                }
            })
            healthIssues = healthIssues.filter(e=>(intersect.find(x=> x===e) === undefined))
        }
      (ingredient as IngredientModel).goodFor = (ingredient as IngredientModel).goodFor.concat(healthIssues); 
      ingredient.save();
      return true;
      }else{
        throw new Error(LogMessages.HEALTH_ISSUE_EMPTY);
      }
    }else{
      throw new Error(LogMessages.INGREDIENT_NOT_EXIST + id);
    }
  }

  public static async addIngredientPicture(id: string, file: Express.Multer.File) {
    let ingredient: IngredientModel;
    try{
      // Try to get the Ingredient from the DB to check it's not already saved in the DB
      ingredient = await Ingredient.findById(id).exec() as IngredientModel;
    }catch(err){
      Logger.error(LogMessages.INGREDIENT_NOT_EXIST + id,err);
      throw new Error(LogMessages.INGREDIENT_NOT_EXIST + id);
    }
    if(ingredient){
      if(file){
        ingredient.picture.data = readFileSync(file.path);
        ingredient.picture.contentType = file.mimetype;
        try{
          ingredient.save();
          return true;
        }catch(err){
          Logger.error(LogMessages.ERROR,err);
          throw new Error(LogMessages.ERROR + " " + err);
        }
      }else{
        throw new Error(LogMessages.FILE_NOT_FOUND);
      } 
    }else{
      throw new Error(LogMessages.INGREDIENT_NOT_EXIST + id);
    }
  }
}