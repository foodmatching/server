export default class GeneralConfig { 
    public projectPath: string;
    public pageSize: number;
    
    public init(path: string,pageSize: number) {
        this.projectPath = path;
        this.pageSize = pageSize;
    }
}