import DBConfig from "./DBConfig";
import LoggerConfig from "./LoggerConfig";
import GeneralConfig from "./GeneralConfig";

export default class Configuration {
    static instance: Configuration;
    DBConfig: DBConfig;
    loggerConfig: LoggerConfig;
    generalConfig: GeneralConfig;

    constructor(dbConfig: DBConfig, loggerConfig: LoggerConfig, generalconfig: GeneralConfig){
        this.DBConfig = dbConfig
        this.loggerConfig = loggerConfig;
        this.generalConfig = generalconfig;
    }

    public static getConfiguration(): Configuration {
        if(!Configuration.instance){
            //Configuration.instance = new Configuration((<any>configurationJson).DBConfig);
        }
        return Configuration.instance;
    }
}