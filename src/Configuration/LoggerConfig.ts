
export default class LoggerConfig { 
    public fileMaxSize: number;// 5MB
    public maxFiles:number;
    public fileLocation:string;
    public fileName: string;
    
    public init(path: string,fileMaxSize: number,maxFiles:number,fileLocation:string,fileName:string) {
        this.fileMaxSize = fileMaxSize;
        this.maxFiles = maxFiles;
        this.fileLocation = fileLocation;
        this.fileName = fileName;

    }
}