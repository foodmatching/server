import express from 'express';
import RoutesInit from './Routes/RoutesInit';
import Http from "http";
import mongo from "connect-mongo";
import mongoose from "mongoose";
import bluebird from "bluebird";
import session from "express-session";
import Configuration from './Configuration/Configuration';
import MiddlewareInit from './Middlewares/MiddlewareInit';
import InitDB from './Utils/InitDB';
import * as DevConfigurationJson from './Configuration/DevConfig.json';
import * as SercerConfigurationJson from './Configuration/ServerConfig.json';
import LogMessages from './Utils/LogMessages';
import {Logger} from './Logs/WinstonLogger';
import DBConfig from './Configuration/DBConfig';
import FileUploader from './Utils/FileUploader';
import IngredientController from './Controllers/IngredientController';
import UsersController from './Controllers/UsersController';
import RecipesService from './services/RecipesService';

import PopulateDB from './Utils/PopulateDB';
import Recipe, { RecipeModel } from './Models/Recipe';
import ImageFinder from './Utils/ImageFinder';
import IngredientService from './services/IngredientService';
import { IngredientModel } from './Models/Ingredient';

export default class Server{
    private static instance: Server;
    private expressServer: Http.Server;
    private static configuration: Configuration;
    private static readonly DEV_ARG = "-dev";
    private static readonly INIT_DB_ARG = "-initdb";
    private static readonly POPULATE_ARG = "-populate";
    private static readonly ADD_PHOTOS = "-photos";
    private constructor (server: Http.Server){
        this.expressServer = server;
    }

    public static async startServer(){
        if(Server.instance == null){
            this.configuration = Server.getConfiguration();
            Logger.InitLogger(this.configuration.loggerConfig,this.configuration.generalConfig.projectPath);

            Logger.info(LogMessages.SERVER_START);
            const app = express();
            let routes = new RoutesInit();
            MiddlewareInit.init(app);
            routes.initRouts(app);
            await Server.initMongo(this.configuration.DBConfig);
            await Server.initDBIfNeeded();

            // await Server.deletePhotosToRecipes();
            await Server.addPhotosToRecipes();

            FileUploader.init(this.configuration.generalConfig.projectPath);
            let expressServer = app.listen(3000, () => {
                Logger.info(LogMessages.SERVER_LISTENING);
                Logger.info(LogMessages.SERVER_IS_UP);
            });
            Server.instance = new Server(expressServer);
        }
    }

    private static async deletePhotosToRecipes(){
        Logger.info("deleteing all photos");
        let recipes: RecipeModel[] = await RecipesService.getAllRecipes(1, 500);
        // let i = 1;
        for(let r of recipes){
            r.picture = undefined;
            await r.save();
        }
        Logger.info("deleteing all photos finished");
    }

    private static async addPhotosToRecipes(){
        Logger.info("starting addPhotosToRecipes");
        if (process.argv.indexOf(Server.ADD_PHOTOS) > -1) {
            let recipes: RecipeModel[] = await RecipesService.getAllRecipes(1, 500);
            recipes = recipes.filter(e=>((!e.picture) || (!e.picture.contentType)|| (e.picture.contentType.split('/')[1]).length<3));
            let i = 1;
            for(let r of recipes){
                if(r.name){
                    let picture;
                    Logger.info(`${i++} of ${recipes.length} addPhotosToRecipes ${r.name}`)
                    try{
                        picture = await ImageFinder.searchPicture(r.name);
                    }catch(err){
                        Logger.error(LogMessages.ERROR,err);
                        return;
                    } 
                    try{
                        if (picture) {
                            Logger.info("photo found :)");
                            (<RecipeModel>r).picture = picture;
                            await r.save();
                        } else {
                            Logger.info("no photo found :(");
                        }
                    }catch(err){
                        Logger.error(LogMessages.ERROR,err);
                    }
                }
            }
        }
    }
    private static async initDBIfNeeded() {
        if (process.argv.indexOf(Server.INIT_DB_ARG) > -1) {
            await InitDB.init();
        }
        if (process.argv.indexOf(Server.POPULATE_ARG) > -1) {
            await PopulateDB.populate(false);
        }
    }

    public initController(defaultPageSize:number){
        RecipesService.init(defaultPageSize);
        IngredientController.init(defaultPageSize);
        UsersController.init(defaultPageSize);
    }
    public static stopServer(){
        if(Server.instance){
            Logger.info(LogMessages.SERVER_STOPPED);
            Server.instance.expressServer.close();
        }
    }

    private static getConfiguration():Configuration {
            if(process.argv.indexOf(Server.DEV_ARG)> -1){
                return DevConfigurationJson as Configuration;
            }else{
                return SercerConfigurationJson as Configuration;
            }
    }
    private static async initMongo(DBConfig : DBConfig){
        const MongoStore = mongo(session);
        // Connect to MongoDB
        const mongoUrl = DBConfig.URL;
        (<any>mongoose).Promise = bluebird;
        try{
            let mongooseInstance = await mongoose.connect(mongoUrl);
            if(mongooseInstance){
                Logger.info(LogMessages.MONGO_CONNECTION_ESTABLISHED);
            }
        }catch(err){
            Logger.error(LogMessages.MONGO_CONNECTION_ERROR, err);
            Server.stopServer;
        }
    }
}

Server.startServer();