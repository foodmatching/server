// controllers/bookController.ts
import { Request, Response } from 'express';
import ErrorToJson from '../Utils/ErrorToJson';
import UsersService from '../services/UsersService';

export default class UsersController{

  
  public static init(defaultPageSize:number){
    UsersService.init(defaultPageSize);
  }
  /**
  * get All Users
  */
  public static async getAllUsers(req: Request, res: Response) {
    let pageNo;
    let size;
    if(req.body.pageNo){
      pageNo = parseInt(req.body.pageNo);
    }
    if(req.body.size){
      size = parseInt(req.body.size);
    }
    try{
        const users = await UsersService.getAllUsers(pageNo, size);
        res.send(users);
      }catch(err){
        res.status(500).send(ErrorToJson.convert(err));
      }
  }

    public static async getUserByID(req: Request, res: Response) {
        try {
            const user = await UsersService.getUserByID(req.body.id);
            res.send(user);
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }

    public static async getUserNameAndPictureByID(req: Request, res: Response) {
        try {
            const user = await UsersService.getUserNameAndPictureByID(req.body.id);
            res.send(user);
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }

    public static async getUserByUserName(req: Request, res: Response) {
        try {
            const user = await UsersService.getUserByUserName(req.body.username);
            res.send(user);
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }
    
    public static async getUserByGmail(req: Request, res: Response) {
        try {
            const user = await UsersService.getUserByGmail(req.body.gmail);
            res.send(user);
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }

    
    public static async getUserByFacebook(req: Request, res: Response) {
        try {
            const user = await UsersService.getUserByFacebook(req.body.facebook);
            res.send(user);
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }

    /**
     * 
     * @param req 
     * @param res 
     */
    public static async deleteUserById(req: Request, res: Response) {
        try {
            const seccesed = await UsersService.deleteUserById(req.body.id);
            res.send(seccesed);
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }

    /**
     * 
     * @param req 
     * @param res 
     */
    public static async deleteUserByUserName(req: Request, res: Response) {
        try {
            const seccesed = await UsersService.deleteUserByUserName(req.body.username);
            res.send(seccesed);
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }

    /**
     * 
     * @param req 
     * @param res 
     */
    public static async connect(req: Request, res: Response) {
        try{
            let identifier: string;
            if(req.body.username){
                identifier = req.body.username;
            }
            else if(req.body.facebook){
                identifier = req.body.facebook;
            }else if(req.body.gmail){
                identifier = req.body.gmail;
            }else if(req.body.email){
                identifier = req.body.email;
            }
            let seccsed = await UsersService.connect(identifier,req.body.password);
            if(seccsed){
                res.send(true);
            }else{
                res.send(false);
            }
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }
    
    public static async addNewUser(req: Request, res: Response){
        try{
            const seccesed: boolean = await UsersService.addNewUser(req.body.user);
            if(seccesed){
              res.send(true);
            }
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }

    public static async addNewUsers(req: Request, res: Response) {
        try{
            const seccesed: boolean = await UsersService.addNewUsers(req.body.users);
            if(seccesed){
              res.send(true);
            }
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }

    public static async addHelthIssues(req: Request, res: Response) {
        try{
            const seccesed: boolean = await UsersService.addHelthIssues(req.body.id,req.body.healthIssues);
            if(seccesed){
              res.send(true);
            } 
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }

    public static async addIngredientsIDsLikes(req: Request, res: Response) {
        try{
            const seccesed: boolean = await UsersService.addIngredientsIDsLikes(req.body.id,req.body.ingredientsIDsLikes);
            if(seccesed){
              res.send(true);
            } 
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }

    /**
     * add types likes
     */
    public static async addTypesLikes(req: Request, res: Response) {
        try{
            const seccesed: boolean = await UsersService.addTypesLikes(req.body.id,req.body.typesLikes);
            if(seccesed){
              res.send(true);
            } 
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }

    public static async addUserPicture(req: Request, res: Response) {
        try{
            const seccesed: boolean =await UsersService.addUserPicture(req.body.id,req.file);
            if(seccesed){
              res.send(true);
            } 
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }

    public static async addCookBook(req: Request, res: Response) {
        try{
            const seccesed: boolean = await UsersService.addCookBook(req.body.id,req.body.name);
            if(seccesed){
              res.send(true);
            } 
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }

    public static async deleteCookBook(req: Request, res: Response) {
        try{
            const seccesed: boolean = await UsersService.deleteCookBook(req.body.id,req.body.cookBookName);
            if(seccesed){
              res.send(true);
            } 
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }

    public static async addRecipeToCookBook(req: Request, res: Response) {
        try{
            const seccesed: boolean = await UsersService.addRecipeToCookBook(req.body.id,req.body.name,req.body.recipeId);
            if(seccesed){
              res.send(true);
            } 
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }

    public static async removeRecipeToCookBook(req: Request, res: Response) {
        try{
            const seccesed: boolean = await UsersService.addRecipeToCookBook(req.body.id,req.body.cookBookName,req.body.recipeId);
            if(seccesed){
              res.send(true);
            } 
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }
    
    public static async getRecipesMatchForUser(req: Request, res: Response) {
        let pageNo;
        let size;
        if(req.body.pageNo){
            pageNo = parseInt(req.body.pageNo);
        }
        if(req.body.size){
            size = parseInt(req.body.size);
        }
        try{
            const recipes = await UsersService.getRecpiesMatchForUser(req.body.id,pageNo,size);
            if(recipes){
              res.send(recipes);
            }
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }

    public static async getAllCookbooks(req: Request, res: Response){
        try{
            const cookBook = await UsersService.getAllCookbooks(req.body.id);
            if(cookBook){
              res.send(cookBook);
            } 
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }

    public static async getAllRecipesInCookBook(req: Request, res: Response) {
        try{
            const recipes = await UsersService.getAllRecipesInCookBook(req.body.id,req.body.name);
            if(recipes){
              res.send(recipes);
            } 
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }
    public static async getAllUserRecipes(req:Request,res:Response){
        try{
            const recipes = await UsersService.getAllUserRecipes(req.body.id);
            if(recipes){
              res.send(recipes);
            } 
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }
    public static async isRecipeRecommendedForUser(req:Request, res:Response){
        try{
            const level: RecommendedLevel = await UsersService.isRecipeRecommendedForUser(req.body.id,req.body.recipeId);
            if(level){
              res.send(level);
            } 
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }

    public static async isIngredientRecommendedForUser(req:Request, res:Response){
        try{
            const level: RecommendedLevel = await UsersService.isIngredientRecommendedForUser(req.body.id,req.body.ingredientId);
            if(level){
              res.send(level);
            } 
        }catch(err){
            res.status(500).send(ErrorToJson.convert(err));
        }
    }
}