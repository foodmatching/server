// controllers/bookController.ts
import { Request, Response } from 'express'
import {Comment} from './../Models/Recipe'
import LogMessages from '../Utils/LogMessages';
import RecipesService from '../services/RecipesService';
import { IngredientModel } from '../Models/Ingredient';
import ErrorToJson from '../Utils/ErrorToJson';

export default class RecipesController{

  public static async getAllRecipes(req: Request, res: Response) {
    let pageNo;
    let size;
    if(req.body.pageNo){
      pageNo = parseInt(req.body.pageNo);
    }
    if(req.body.size){
      size = parseInt(req.body.size);
    }
    try{
      const recipes = await RecipesService.getAllRecipes(pageNo, size);
      res.send(recipes);
    }catch(err){
      res.status(500).send(ErrorToJson.convert(err));
    }
  }
  public static async getRecipesContaining(req: Request, res: Response){
    let pageNo;
    let size;
    if(req.body.pageNo){
      pageNo = parseInt(req.body.pageNo);
    }
    if(req.body.size){
      size = parseInt(req.body.size);
    }try{
      const recipes = await RecipesService.getRecipesContaining(pageNo, size,req.body.name);
      res.send(recipes);
    }catch(err){
      res.status(500).send(ErrorToJson.convert(err));
    }
}
public static async getRecipesContainingNamesAndIds(req: Request, res: Response){
  let pageNo;
  let size;
  if(req.body.pageNo){
    pageNo = parseInt(req.body.pageNo);
  }
  if(req.body.size){
    size = parseInt(req.body.size);
  }try{
    const recipesNames = await RecipesService.getRecipesContainingNamesAndIds(pageNo, size,req.body.name);
    res.send(recipesNames);
  }catch(err){
    res.status(500).send(ErrorToJson.convert(err));
  }
}

  /**
   * Get the specific recipe by it's ID
   * @param req 
   * @param res 
   */
   public static async getRecipeByID(req: Request, res: Response) {
      try {
        const recipe = await RecipesService.getRecipeByID(req.body.id);
        res.send(recipe);
      }catch(err){
        res.status(500).send(ErrorToJson.convert(err));
      }
    }

  /**
   * 
   * @param req 
   * @param res 
   */
  public static async getRecipeByName(req: Request, res: Response) {
    try {
      const recipe = await RecipesService.getRecipeByName(req.body.id);
      res.send(recipe);
    }catch(err){
      res.status(500).send(ErrorToJson.convert(err));
    }
  }

  public static async deleteRecipeById(req: Request, res: Response) {
    try{
      const seccesed = await RecipesService.deleteRecipeById(req.body.id);
      if(seccesed){
        res.send(true);
      }else{
        throw new Error(LogMessages.ERROR);
      }
    }catch(err){
      res.status(500).send(ErrorToJson.convert(err));
    }
  }

  public static async deleteRecipeByName(req: Request, res: Response) {
    try{
      const seccesed = await RecipesService.deleteRecipeByName(req.body.name);
      if(seccesed){
        res.send(true);
      }else{
        throw new Error(LogMessages.ERROR);
      }
    }catch(err){
      res.status(500).send(ErrorToJson.convert(err));
    }
  }

  public static async addRecipe(req: Request, res: Response){
    try{
      const recipeId: string = await RecipesService.addRecipe(req.body.recipe);
      if(recipeId){
        res.send(recipeId);
      }
      else{
        res.status(500).send();
      }
    }catch(err){
      res.status(500).send(ErrorToJson.convert(err));
    }
  }

  public static async addRecipes(req: Request, res: Response) {
    try{
      const seccesed: boolean =await RecipesService.addRecipes(req.body.recipes);
      if(seccesed){
        res.send(true);
      } 
    }catch(err){
      res.status(500).send(ErrorToJson.convert(err));
    }
    
  }

  public static async addRecipePicture(req: Request, res: Response) {
    try{
      const seccesed: boolean = await RecipesService.addRecipePicture(req.body.id,req.file);
      if(seccesed){
        res.send(true);
      }
    }catch(err){
      res.status(500).send(ErrorToJson.convert(err));
    }
  }

  public static async addRecipeCommentPicture(req: Request, res: Response) {
    try{
      const seccesed: boolean = await RecipesService.addRecipeCommentPicture(req.body.id,req.body.commentId,req.file);
      if(seccesed){
        res.send(true);
      }
    }catch(err){
      res.status(500).send(ErrorToJson.convert(err));
    }
  }
  
  public static async addCommentToRecipe(req: Request, res: Response){
    try{
      const commentId: string = await RecipesService.addCommentToRecipe(req.body.id,req.body.comment);
      res.send(commentId);
    }catch(err){
      res.status(500).send(ErrorToJson.convert(err));
    } 
  }
  
  public static async getAllCommentsForRecipe(req: Request, res: Response){
    try{
      const comments: Comment[] = await RecipesService.getAllCommentsForRecipe(req.body.id);
      res.send(comments);
    }catch(err){
      res.status(500).send(ErrorToJson.convert(err));
    } 
  }
  
  public static async getIngredientsForRecipe(req: Request,res :Response){
    try{
      let ingredients :IngredientModel[] = await RecipesService.getIngredients(req.body.id);
      res.send(ingredients);
    }catch(err){
      res.status(500).send(ErrorToJson.convert(err));
    }
  }
}