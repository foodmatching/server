import {Request, Response} from 'express';
import LogMessages from '../Utils/LogMessages';
import IngredientService from '../services/IngredientService';
import ErrorToJson from '../Utils/ErrorToJson';

export default class IngredientController {

  public static init(defaultPageSize:number){
    IngredientService.init(defaultPageSize);
  }
  
  /**
  * getAllIngredients
  */
  public static async getAllIngredients(req: Request, res: Response) {
    try{
      let pageNo;
      let size;
      if(req.body.pageNo){
        pageNo = parseInt(req.body.pageNo);
      }
      if(req.body.size){
        size = parseInt(req.body.size);
      }
      let ingredients =await IngredientService.getAllIngredients(pageNo,size);
      res.send(ingredients);
    }catch(err){
      res.status(500).send(ErrorToJson.convert(err));
    }
  }
 
  public static async getIngredientByID(req: Request, res: Response) {
    try{
        const ingredient = await IngredientService.getIngredientByID(req.body.id);
        res.send(ingredient);
      }catch(err){
        res.status(500).send(ErrorToJson.convert(err));
      }
    }

  public static async getIngredientByName(req: Request, res: Response) {
      try{
        let ingredient = await IngredientService.getIngredientByName(req.body.name);
        res.send(ingredient);
    }catch(err){
      res.status(500).send(ErrorToJson.convert(err));
    }
  }

  public static async getIngredientStartWith(req: Request, res: Response){
      let pageNo;
      let size;
      if(req.body.pageNo){
        pageNo = parseInt(req.body.pageNo);
      }
      if(req.body.size){
        size = parseInt(req.body.size);
      }try{
        const recipes = await IngredientService.getIngredientStartWith(pageNo, size,req.body.name);
        res.send(recipes);
      }catch(err){
        res.status(500).send(ErrorToJson.convert(err));
      }
  }

  public static async deleteIngredientById(req: Request, res: Response) {
    try{
      const seccesed = await IngredientService.deleteIngredientById(req.body.id);
      if(seccesed){
        res.send(true);
      }else{
        throw new Error(LogMessages.ERROR);
      }
    }catch(err){
      res.status(500).send(ErrorToJson.convert(err));
    }
  }

  /**
   * getSpesificIngredient
   */
  public static async deleteIngredientByName(req: Request, res: Response) {
    try{
      const seccesed = await IngredientService.deleteIngredientByName(req.body.name);
      if(seccesed){
        res.send(true);
      }else{
        throw new Error(LogMessages.ERROR);
      }
    }catch(err){
      res.status(500).send(ErrorToJson.convert(err));
    }
  }

  public static async addIngredient(req: Request, res: Response){
    try{
      const seccesed: boolean = await IngredientService.addIngredient(req.body.ingredient);
      if(seccesed){
        res.send(true);
      }
    }catch(err){
      res.status(500).send(ErrorToJson.convert(err));
    }
  }

  /**
   * addIngredients
   */
  public static async addIngredients(req: Request, res: Response) {
    try{
      const seccesed: boolean =await IngredientService.addIngredients(req.body.ingredients);
      if(seccesed){
        res.send(true);
      } 
    }catch(err){
      res.status(500).send(ErrorToJson.convert(err));
    }
  }

  public static async addBadFor(req: Request, res: Response) {
    try{
      const seccesed: boolean =await IngredientService.addBadFor(req.body.id,req.body.healthIssues);
      if(seccesed){
        res.send(true);
      } 
    }catch(err){
      res.status(500).send(ErrorToJson.convert(err));
    }
  }
  public static async addGoodFor(req: Request, res: Response) {
    try{
      const seccesed: boolean =await IngredientService.addGoodFor(req.body.id,req.body.healthIssues);
      if(seccesed){
        res.send(true);
      } 
    }catch(err){
      res.status(500).send(ErrorToJson.convert(err));
    }
  }
  public static async addIngredientPicture(req: Request, res: Response) {
    try{
      const seccesed: boolean =await IngredientService.addIngredientPicture(req.body.id,req.file);
      if(seccesed){
        res.send(true);
      } 
    }catch(err){
      res.status(500).send(ErrorToJson.convert(err));
    }
  }
}